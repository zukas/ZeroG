#include <common/zerog_def.h>

#include <engine.h>
#include <renderer/renderer.h>

//#include <screen/window.h>
#include <screen/window_events.h>
#include <screen/window_system.h>

#include <renderer/camera.h>
#include <renderer/data_types.h>
#include <renderer/utils.h>

#include "common/file.h"

#include <common/clock.h>

#include <renderer/debug.h>
#include <renderer/entity.h>
#include <renderer/meterial.h>
#include <renderer/renderer_state.h>

#include <image/image_source.h>

#include <logger/logger.h>
#include <memory/alloc.h>

#include <glm/gtc/matrix_transform.hpp>

#include <algorithm>
#include <cassert>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <random>

#define PI 3.14159265358979323846f

struct model_data_pos_normal_uv {
  glm::vec3 *positions;
  glm::vec3 *normals;
  glm::vec2 *uvs;
  int32_t count;
};

struct model_indexes {
  uint32_t *indexes;
  int32_t count;
};

// constexpr ZeroG::mesh_3d_rgb_data g_vertex_buffer_data[] = {
//    {{-1.0f, -1.0f, -1.0f}, {1, 0, 0}}, {{-1.0f, -1.0f, 1.0f}, {0, 1, 0}},
//    {{-1.0f, 1.0f, 1.0f}, {0, 0, 1}}, // Left Side
//    {{-1.0f, -1.0f, -1.0f}, {1, 0, 0}}, {{-1.0f, 1.0f, 1.0f}, {0, 1, 0}},
//    {{-1.0f, 1.0f, -1.0f}, {0, 0, 1}}, // Left Side
//    {{1.0f, 1.0f, -1.0f}, {1, 0, 0}},   {{-1.0f, -1.0f, -1.0f}, {0, 1, 0}},
//    {{-1.0f, 1.0f, -1.0f}, {0, 0, 1}}, // Back Side
//    {{1.0f, -1.0f, 1.0f}, {1, 0, 0}},   {{-1.0f, -1.0f, -1.0f}, {0, 1, 0}},
//    {{1.0f, -1.0f, -1.0f}, {0, 0, 1}}, // Bottom Side
//    {{1.0f, 1.0f, -1.0f}, {1, 0, 0}},   {{1.0f, -1.0f, -1.0f}, {0, 1, 0}},
//    {{-1.0f, -1.0f, -1.0f}, {0, 0, 1}}, // Back Side
//    {{1.0f, -1.0f, 1.0f}, {1, 0, 0}},   {{-1.0f, -1.0f, 1.0f}, {0, 1, 0}},
//    {{-1.0f, -1.0f, -1.0f}, {0, 0, 1}}, // Bottom Side
//    {{-1.0f, 1.0f, 1.0f}, {1, 0, 0}},   {{-1.0f, -1.0f, 1.0f}, {0, 1, 0}},
//    {{1.0f, -1.0f, 1.0f}, {0, 0, 1}}, // Front Side
//    {{1.0f, 1.0f, 1.0f}, {1, 0, 0}},    {{1.0f, -1.0f, -1.0f}, {0, 1, 0}},
//    {{1.0f, 1.0f, -1.0f}, {0, 0, 1}}, // Right Side
//    {{1.0f, -1.0f, -1.0f}, {1, 0, 0}},  {{1.0f, 1.0f, 1.0f}, {0, 1, 0}},
//    {{1.0f, -1.0f, 1.0f}, {0, 0, 1}}, // Right Side
//    {{1.0f, 1.0f, 1.0f}, {1, 0, 0}},    {{1.0f, 1.0f, -1.0f}, {0, 1, 0}},
//    {{-1.0f, 1.0f, -1.0f}, {0, 0, 1}}, // Top Side
//    {{1.0f, 1.0f, 1.0f}, {1, 0, 0}},    {{-1.0f, 1.0f, -1.0f}, {0, 1, 0}},
//    {{-1.0f, 1.0f, 1.0f}, {0, 0, 1}}, // Top Side
//    {{1.0f, 1.0f, 1.0f}, {1, 0, 0}},    {{-1.0f, 1.0f, 1.0f}, {0, 1, 0}},
//    {{1.0f, -1.0f, 1.0f}, {0, 0, 1}} // Front Side
//};

namespace cube {
constexpr uint32_t vertex_count() { return 24; }
constexpr uint32_t index_count() { return 36; }

// void generate(ZeroG::mesh_3d_rgb_data *vertexes, uint32_t *indexes) {
//  uint32_t idx[]{0,  1,  2,  0,  2,  3,  // front
//                 4,  5,  6,  4,  6,  7,  // right
//                 8,  9,  10, 8,  10, 11, // back
//                 12, 13, 14, 12, 14, 15, // left
//                 16, 17, 18, 16, 18, 19, // upper
//                 20, 21, 22, 20, 22, 23};
//  ZeroG::mesh_3d_rgb_data vert[]{
//      // FRONT
//      {glm::vec3(-1.0, -1.0, 1.0), glm::vec3(1, 0, 0)},
//      {glm::vec3(1.0, -1.0, 1.0), glm::vec3(1, 0, 0)},
//      {glm::vec3(1.0, 1.0, 1.0), glm::vec3(1, 0, 0)},
//      {glm::vec3(-1.0, 1.0, 1.0), glm::vec3(1, 0, 0)},

//      // RIGHT
//      {glm::vec3(1.0, -1.0, 1.0), glm::vec3(0, 1, 0)},
//      {glm::vec3(1.0, -1.0, -1.0), glm::vec3(0, 1, 0)},
//      {glm::vec3(1.0, 1.0, -1.0), glm::vec3(0, 1, 0)},
//      {glm::vec3(1.0, 1.0, 1.0), glm::vec3(0, 1, 0)},

//      // BACK
//      {glm::vec3(-1.0, -1.0, -1.0), glm::vec3(0, 0, 1)}, //-1.0, -1.0, -1.0 :
//      1 {glm::vec3(-1.0, 1.0, -1.0), glm::vec3(0, 0, 1)},  // 1.0, -1.0, -1.0
//      : 4 {glm::vec3(1.0, 1.0, -1.0), glm::vec3(0, 0, 1)},   // 1.0, 1.0, -1.0
//      : 3 {glm::vec3(1.0, -1.0, -1.0), glm::vec3(0, 0, 1)},  //-1.0, 1.0, -1.0
//      : 2

//      // LEFT
//      {glm::vec3(-1.0, -1.0, -1.0), glm::vec3(1, 0, 1)},
//      {glm::vec3(-1.0, -1.0, 1.0), glm::vec3(1, 0, 1)},
//      {glm::vec3(-1.0, 1.0, 1.0), glm::vec3(1, 0, 1)},
//      {glm::vec3(-1.0, 1.0, -1.0), glm::vec3(1, 0, 1)},

//      // TOP
//      {glm::vec3(1.0, 1.0, 1.0), glm::vec3(1, 1, 0)},
//      {glm::vec3(1.0, 1.0, -1.0), glm::vec3(1, 1, 0)},
//      {glm::vec3(-1.0, 1.0, -1.0), glm::vec3(1, 1, 0)},
//      {glm::vec3(-1.0, 1.0, 1.0), glm::vec3(1, 1, 0)},

//      // BOTTOM
//      {glm::vec3(-1.0, -1.0, -1.0), glm::vec3(0, 1, 1)},
//      {glm::vec3(1.0, -1.0, -1.0), glm::vec3(0, 1, 1)},
//      {glm::vec3(1.0, -1.0, 1.0), glm::vec3(0, 1, 1)},
//      {glm::vec3(-1.0, -1.0, 1.0), glm::vec3(0, 1, 1)},
//  };

//  memcpy(vertexes, vert, sizeof(vert));
//  memcpy(indexes, idx, sizeof(idx));
//}

} // namespace cube

namespace sphare {

constexpr uint32_t vertex_count(uint32_t units) {
  return (units + 1) * (units + 1);
}
constexpr uint32_t index_count(uint32_t units) {
  return (units) * (units - 1) * 6;
}

// void generate(ZeroG::mesh_3d_uv_normal_data *vertexes, uint32_t *indexes,
//              uint32_t units) {

//  int64_t steps = units;

//  float x, y, z, xy; // vertex position
//  float nx, ny, nz;  // vertex normal
//  float s, t;        // vertex texCoord

//  const float sectorStep = 2.f * PI / float(steps);
//  const float stackStep = PI / float(steps);
//  float sectorAngle, stackAngle;

//  int64_t index = 0;

//  for (int64_t i = 0; i <= steps; ++i) {
//    stackAngle = PI / 2.f - i * stackStep; // starting from pi/2 to -pi/2
//    xy = cosf(stackAngle);                 // r * cos(u)
//    z = sinf(stackAngle);                  // r * sin(u)

//    for (int64_t j = 0; j <= units; ++j) {
//      sectorAngle = j * sectorStep;

//      x = xy * cosf(sectorAngle); // r * cos(u) * cos(v)
//      y = xy * sinf(sectorAngle); // r * cos(u) * sin(v)

//      nx = x;
//      ny = y;
//      nz = z;

//      s = float(j) / float(steps);
//      t = float(i) / float(steps);

//      vertexes[index].xyz = glm::vec3{x, y, z};
//      vertexes[index].uv = glm::vec2{s, t};
//      vertexes[index].normal = glm::vec3{nx, ny, nz};
//      ++index;
//    }
//  }

//  uint32_t k1 = 0;
//  uint32_t k2 = 0;

//  index = 0;

//  for (int64_t i = 0; i < steps; ++i) {
//    k1 = static_cast<uint32_t>(i * (steps + 1)); // beginning of current stack
//    k2 = static_cast<uint32_t>(k1 + steps + 1);  // beginning of next stack

//    for (int64_t j = 0; j < steps; ++j, ++k1, ++k2) {
//      // 2 triangles per sector excluding first and last stacks
//      // k1 => k2 => k1+1
//      if (i != 0) {
//        indexes[index++] = k1;
//        indexes[index++] = k2;
//        indexes[index++] = k1 + 1;
//      }

//      // k1+1 => k2 => k2+1
//      if (i != (steps - 1)) {
//        indexes[index++] = k1 + 1;
//        indexes[index++] = k2;
//        indexes[index++] = k2 + 1;
//      }
//    }
//  }

//  ZeroG::logger::log("index: %ld", index);
//}

void generate(model_data_pos_normal_uv &vertex_data, model_indexes &idx_data,
              uint32_t units) {
  int64_t steps = units;

  float x, y, z, xy; // vertex position
  float nx, ny, nz;  // vertex normal
  float s, t;        // vertex texCoord

  const float sectorStep = 2.f * PI / float(steps);
  const float stackStep = PI / float(steps);
  float sectorAngle, stackAngle;

  int64_t index = 0;

  for (int64_t i = 0; i <= steps; ++i) {
    stackAngle = PI / 2.f - i * stackStep; // starting from pi/2 to -pi/2
    xy = cosf(stackAngle);                 // r * cos(u)
    z = sinf(stackAngle);                  // r * sin(u)

    for (int64_t j = 0; j <= units; ++j) {
      sectorAngle = j * sectorStep;

      x = xy * cosf(sectorAngle); // r * cos(u) * cos(v)
      y = xy * sinf(sectorAngle); // r * cos(u) * sin(v)

      nx = x;
      ny = y;
      nz = z;

      s = float(j) / float(steps);
      t = float(i) / float(steps);

      vertex_data.positions[index] = glm::vec3{x, y, z};
      vertex_data.normals[index] = glm::vec3{nx, ny, nz};
      vertex_data.uvs[index] = glm::vec2{s, t};
      ++index;
    }
  }

  uint32_t k1 = 0;
  uint32_t k2 = 0;

  index = 0;

  for (int64_t i = 0; i < steps; ++i) {
    k1 = static_cast<uint32_t>(i * (steps + 1)); // beginning of current stack
    k2 = static_cast<uint32_t>(k1 + steps + 1);  // beginning of next stack

    for (int64_t j = 0; j < steps; ++j, ++k1, ++k2) {
      // 2 triangles per sector excluding first and last stacks
      // k1 => k2 => k1+1
      if (i != 0) {
        idx_data.indexes[index++] = k1;
        idx_data.indexes[index++] = k2;
        idx_data.indexes[index++] = k1 + 1;
      }

      // k1+1 => k2 => k2+1
      if (i != (steps - 1)) {
        idx_data.indexes[index++] = k1 + 1;
        idx_data.indexes[index++] = k2;
        idx_data.indexes[index++] = k2 + 1;
      }
    }
  }
}

} // namespace sphare

void init_model_mat(glm::mat4 *matrixes, int32_t count) {
  for (int32_t i = 0; i < count; ++i) {
    matrixes[i] = glm::mat4(1.f);
  }
}

void create_instancing_transforms(glm::mat4 *matrixes, int32_t count) {

  std::random_device
      rd; // Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); // Standard mersenne_twister_engine seeded with rd()
  std::uniform_real_distribution<> dis(0, 1000.0);

  std::uniform_real_distribution<> dis2(1.17, 2.75);

  for (int32_t i = 0; i < count; ++i) {

    float x = (float(dis(gen)) - 550.f) * float(dis2(gen));
    float y = (float(dis2(gen)) * float(dis2(gen)) - 2.f) * float(dis2(gen));
    float z = (float(dis(gen)) - 550.f) * float(dis2(gen));
    glm::mat4 scale = glm::scale(glm::mat4(1.f), glm::vec3(10.f, 10.f, 10.f));
    glm::mat4 rot =
        glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(1, 0, 0));
    glm::mat4 move = glm::translate(glm::mat4(1.f), glm::vec3(x, y, z));
    matrixes[i] = move * rot * scale;
  }
}

void update_instancing_transforms(glm::mat4 *transforms, int32_t count) {

  constexpr float pi_scale = PI / 10000000000.f;
  for (int32_t i = 0; i < count; ++i) {
    transforms[i] = glm::rotate(
        transforms[i], pi_scale * float(ZeroG::clock::game_time_delta()),
        glm::vec3(0, 0, 1));
  }
}

struct Matrixes {
  glm::mat4 view;
  glm::mat4 projection;
};

struct control_block {
  uint32_t window_id;
  bool is_closing;
};

void process_events(uint32_t window_id, const ZeroG::event_t *events,
                    int32_t length, const ZeroG::key_state_t *keys,
                    void *user_data) {
  control_block *block = static_cast<control_block *>(user_data);

  if (block->window_id != window_id) {
    ZeroG::logger::log(
        "Event processor for window %u invoked for incorrect window id %u",
        block->window_id, window_id);
    return;
  }

  ZeroG::logger::log("Processing winddow event for %u, number of events %d",
                     block->window_id, length);

  constexpr void *event_handles[]{
      [ZeroG::value(ZeroG::event_type_t::WINDOW_SHOWN)] = &&WINDOW_SHOWN_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_HIDDEN)] = &&WINDOW_HIDDEN_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_EXPOSED)] =
          &&WINDOW_EXPOSED_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_MOVED)] = &&WINDOW_MOVED_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_RESIZED)] =
          &&WINDOW_RESIZED_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_SIZE_CHANGED)] =
          &&WINDOW_SIZE_CHANGED_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_MINIMIZED)] =
          &&WINDOW_MINIMIZED_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_MAXIMIZED)] =
          &&WINDOW_MAXIMIZED_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_RESTORED)] =
          &&WINDOW_RESTORED_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_ENTER)] = &&WINDOW_ENTER_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_LEAVE)] = &&WINDOW_LEAVE_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_FOCUS_GAINED)] =
          &&WINDOW_FOCUS_GAINED_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_FOCUS_LOST)] =
          &&WINDOW_FOCUS_LOST_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_CLOSE)] = &&WINDOW_CLOSE_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_TAKE_FOCUS)] =
          &&WINDOW_TAKE_FOCUS_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_HIT_TEST)] =
          &&WINDOW_HIT_TEST_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_KEY)] = &&WINDOW_KEY_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_MOUSE_MOVE)] =
          &&WINDOW_MOUSE_MOVE_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_MOUSE_WHEEL)] =
          &&WINDOW_MOUSE_WHEEL_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_MOUSE_BUTTON)] =
          &&WINDOW_MOUSE_BUTTON_LBL,
      [ZeroG::value(ZeroG::event_type_t::WINDOW_TEXT_EDIT)] =
          &&WINDOW_TEXT_EDIT_LBL,
      [ZeroG::value(ZeroG::event_type_t::END_STREAM)] = &&END_STREAM_LBL,
  };

  const ZeroG::event_t *eve = nullptr;
  const auto next = [&]() {
    eve = events;
    auto type = eve->type;
    events++;
    return event_handles[value(type)];
  };
  goto *next();

WINDOW_SHOWN_LBL:
  goto *next();

WINDOW_HIDDEN_LBL:
  goto *next();

WINDOW_EXPOSED_LBL:
  goto *next();

WINDOW_MOVED_LBL:
  goto *next();

WINDOW_RESIZED_LBL:
  goto *next();

WINDOW_SIZE_CHANGED_LBL:
  goto *next();

WINDOW_MINIMIZED_LBL:
  goto *next();

WINDOW_MAXIMIZED_LBL:
  goto *next();

WINDOW_RESTORED_LBL:
  goto *next();

WINDOW_ENTER_LBL:
  goto *next();

WINDOW_LEAVE_LBL:
  goto *next();

WINDOW_FOCUS_GAINED_LBL:
  goto *next();

WINDOW_FOCUS_LOST_LBL:
  goto *next();

WINDOW_CLOSE_LBL:
  ZeroG::logger::log("Window closing %u", block->window_id);
  block->is_closing = true;
  goto *next();

WINDOW_TAKE_FOCUS_LBL:
  goto *next();

WINDOW_HIT_TEST_LBL:
  goto *next();

WINDOW_KEY_LBL:
  goto *next();

WINDOW_MOUSE_MOVE_LBL:
  goto *next();

WINDOW_MOUSE_WHEEL_LBL:
  goto *next();

WINDOW_MOUSE_BUTTON_LBL:
  goto *next();

WINDOW_TEXT_EDIT_LBL:
  goto *next();

END_STREAM_LBL:
  return;
}

int main(int argc, char **argv) {

  ZeroG::memory::stack_allocator_create_info_t alloc_info;
  alloc_info.parent_allocator = nullptr;
  alloc_info.size = ZeroG::memory::GB;
  alloc_info.alignment = 16;

  ZeroG::memory::allocator_t *alloc = ZeroG::memory::create(alloc_info);
  ZeroG::logger::init_logger(alloc, ZeroG::memory::KB * 64);

  int32_t win_width = 2048;
  int32_t win_height = 1280;
  {

    ZeroG::window_system window_system(alloc);
    window_system.frame_limiter(ZeroG::toggle::OFF);

    control_block data_block{.is_closing = false};

    ZeroG::window_info_t main_window_info;
    main_window_info.width = win_width;
    main_window_info.height = win_height;
    main_window_info.window_title = "ZeroG";
    main_window_info.event_handler = process_events;
    main_window_info.user_data = &data_block;

    data_block.window_id = window_system.create_window(
        main_window_info); // ZeroG::window window(win_width, win_height,
                           // "ZeroG");

    ZeroG::debug_init();

    File vert_shader_source("shaders/basic2.vert");
    size_t vert_size = vert_shader_source.size();
    ZeroG::memory::block vert_blk = ZeroG::memory::allocate(alloc, vert_size);
    char *vert = static_cast<char *>(vert_blk.address);
    vert_shader_source.read(vert, vert_size);

    File frag_shader_source("shaders/basic2.frag");
    size_t frag_size = frag_shader_source.size();
    ZeroG::memory::block frag_blk = ZeroG::memory::allocate(alloc, frag_size);
    char *frag = static_cast<char *>(frag_blk.address);
    frag_shader_source.read(frag, frag_size);

    ZeroG::logger::log("************** Begin Vertex Shader "
                       "**************\n%.*s\n*************** End Vertex "
                       "Shader ***************",
                       vert_size, vert);
    ZeroG::logger::log("************** Begin Fragment Shader "
                       "**************\n%.*s\n*************** End Fragment "
                       "Shader ***************",
                       frag_size, frag);
    ZeroG::entity e;

    ZeroG::shader_source_t shaders[]{
        {vert, static_cast<uint32_t>(vert_size), ZeroG::shader_type::VERTEX},
        {frag, static_cast<uint32_t>(frag_size), ZeroG::shader_type::FRAGMENT}};

    e.program_id = ZeroG::program::create(shaders, 2);

    ZeroG::memory::deallocate(alloc, frag_blk);
    ZeroG::memory::deallocate(alloc, vert_blk);

    constexpr uint32_t instance_count = 100000;
    constexpr uint32_t units = 18;
    constexpr uint32_t vertex_count = sphare::vertex_count(units);
    constexpr uint32_t index_count = sphare::index_count(units);
    struct m_vert {
      glm::vec3 positions[vertex_count];
      glm::vec3 normals[vertex_count];
      glm::vec2 uvs[vertex_count];
      //    const int32_t count{vertex_count};
    } vert_data;

    struct m_idx {
      uint32_t indexes[index_count];
      //    int32_t count{index_count};
    } idx_data;

    model_data_pos_normal_uv vert_tmp{vert_data.positions, vert_data.normals,
                                      vert_data.uvs, 0};

    model_indexes idx_tmp{idx_data.indexes, 0};

    //  ZeroG::mesh_3d_uv_normal_data buffer_data[vertex_count];
    //  uint32_t index_data[index_count];

    sphare::generate(vert_tmp, idx_tmp, units);

    ZeroG::memory::block model_mat_blk =
        ZeroG::memory::allocate(alloc, instance_count * sizeof(glm::mat4));
    glm::mat4 *instanced_model_mat2 =
        static_cast<glm::mat4 *>(model_mat_blk.address);
    create_instancing_transforms(instanced_model_mat2, instance_count);

    ZeroG::memory::block anim_mat_blk =
        ZeroG::memory::allocate(alloc, instance_count * sizeof(glm::mat4));
    glm::mat4 *instanced_anim_mat2 =
        static_cast<glm::mat4 *>(anim_mat_blk.address);
    init_model_mat(instanced_anim_mat2, instance_count);

    ZeroG::memory::block mvp_blk =
        ZeroG::memory::allocate(alloc, instance_count * sizeof(glm::mat4));
    glm::mat4 *model = static_cast<glm::mat4 *>(mvp_blk.address);
    for (int32_t i = 0, c = instance_count; i < c; ++i) {
      model[i] = instanced_model_mat2[i] * instanced_anim_mat2[i];
    }

    const ZeroG::buffer_create_info_t buffer_infos[]{
        {ZeroG::buffer_type_e::STATIC, sizeof(m_vert), &vert_data},
        {ZeroG::buffer_type_e::STATIC, sizeof(m_idx), &idx_data},
        {ZeroG::buffer_type_e::DYNAMIC, instance_count * sizeof(glm::mat4),
         model}};

    uint32_t buffers[3]{0};
    ZeroG::buffer::create(buffer_infos, 3, buffers);

    constexpr ZeroG::vertex_buffer_section_definition_t per_vertex[3]{
        {ZeroG::data_type_e::FLOAT_VECTOR3},
        {ZeroG::data_type_e::FLOAT_VECTOR3},
        {ZeroG::data_type_e::FLOAT_VECTOR2}};

    constexpr ZeroG::vertex_buffer_section_definition_t per_instance[2]{
        {ZeroG::data_type_e::FLOAT_MATRIX4}};

    const ZeroG::vertex_buffer_definition_t infos[2]{
        {buffers[0], ZeroG::data_rate_e::PER_VERTEX, vertex_count, 3,
         per_vertex},
        {buffers[2], ZeroG::data_rate_e::PER_INSTANCE, instance_count, 1,
         &per_instance[0]}};

    const ZeroG::vertex_array_definition_t varray_info{infos, 2, buffers[1]};

    ZeroG::varray::create(&varray_info, 1, &e.varray_id);
    //  e.varray_id = ZeroG::varray::create<
    //      ZeroG::vertex_buffer_desc<ZeroG::mesh_3d_uv_normal_data>,
    //      ZeroG::vertex_buffer_desc<glm::mat4>>(buffers[0], buffers[2],
    //      buffers[1]);

    e.count = index_count;
    e.render_type = ZeroG::entity::INDEX_INSTANCED;
    e.instance_count = instance_count;

    ZeroG::image_source::image img =
        ZeroG::image_source::load("images/earth.jpg");

    ZeroG::param_info_t param_infos[2]{
        ZeroG::param_wrap_t{ZeroG::parameter_type::WRAP_S,
                            ZeroG::wrap_e::CLAMP_TO_EDGE},
        ZeroG::param_wrap_t{ZeroG::parameter_type::WRAP_T,
                            ZeroG::wrap_e::CLAMP_TO_EDGE}};

    uint8_t *img_data = static_cast<uint8_t *>(img.data);
    std::reverse(img_data, img_data + (img.width + img.height));

    ZeroG::texture_create_info_t texture_info{
        img.width, img.height, img.format, 2, img_data, param_infos};

    ZeroG::sampler_create_info_t sampler_info{2, param_infos};

    uint32_t texture_id = 0;
    uint32_t sampler_id = 0;

    ZeroG::texture::create_2d(&texture_info, 1, &texture_id);
    ZeroG::sampler::create(&sampler_info, 1, &sampler_id);

    ZeroG::image_source::unload(img);

    ZeroG::uniform::set_value(e.program_id, 11, 0);

    ZeroG::texture::bind(0, 1, &texture_id);
    ZeroG::sampler::bind(0, 1, &sampler_id);

    ZeroG::camera cam{glm::vec3(120.0f, 20.0f, 120.0f),
                      glm::vec3(0.0f, -0.45f, -1.0f),
                      glm::vec3(0.0f, 1.0f, 0.0f)};

    Matrixes m{ZeroG::camera_view(cam),
               glm::perspective(glm::radians(45.f),
                                float(win_width) / float(win_height), 0.01f,
                                1000.0f)};
    //  glm::mat4 VP = m.projection * m.view;
    //  uint32_t ubuf{0};

    //  //  m.model = glm::scale(m.model, glm::vec3(4, 4, 4));

    //  const ZeroG::buffer_create_info_t uinfo{ZeroG::buffer_type::STATIC,
    //                                          sizeof(VP), &VP};

    //  ZeroG::buffer::create(&uinfo, 1, &ubuf);

    //  ZeroG::uniform::resolve_block(e.program_id, "Matrixes", 0);
    //  ZeroG::uniform::bind_buffer(0, ubuf);

    ZeroG::buffer_update_info_t bui_model[]{
        //      {ubuf, 0, sizeof(VP), &VP},
        {buffers[2], 0, instance_count / 5 * sizeof(glm::mat4), model}};

    ZeroG::clock::init();
    ZeroG::clock::set_game_speed(1);

    ZeroG::clock::update();

    int64_t time_sum = 0;

    ZeroG::renderer_change state[]{{{ZeroG::DEPTH_TEST, ZeroG::toggle::ON}},
                                   {{ZeroG::CULL_FACE, ZeroG::toggle::ON}},
                                   {{ZeroG::DEPTH_TEST_FUNC, ZeroG::LESS}},
                                   {{ZeroG::CULL_FACE_FUNC, ZeroG::BACK}}};

    ZeroG::execute_renderer_change(state, 4);

    bool force_exit = false;

    //  ZeroG::input_handler<void> ex{window, [&force_exit]() { force_exit =
    //  true;
    //  },
    //                                ZeroG::input::key::KEY_BACKSPACE,
    //                                ZeroG::input::mod_bits::LCTRL,
    //                                ZeroG::input::action::RELEASE};

    //  ZeroG::input_handler<void> mv_f(
    //      window, [&cam]() { cam = ZeroG::move_parallel(cam, 1.5f); },
    //      ZeroG::input::key::KEY_UP);

    //  ZeroG::input_handler<void> mv_b(
    //      window, [&cam]() { cam = ZeroG::move_parallel(cam, -1.5f); },
    //      ZeroG::input::key::KEY_DOWN);

    //  ZeroG::input_handler<void> mv_l(
    //      window, [&cam]() { cam = ZeroG::move_perpendicular(cam, -1.5f); },
    //      ZeroG::input::key::KEY_LEFT);

    //  ZeroG::input_handler<void> mv_r(
    //      window, [&cam]() { cam = ZeroG::move_perpendicular(cam, 1.5f); },
    //      ZeroG::input::key::KEY_RIGHT);

    //  ZeroG::input_handler<void> rr_cam(
    //      window, [&cam]() { cam = ZeroG::rotate_horizontally(cam, PI /
    //      135.f);
    //      }, ZeroG::input::key::KEY_Q);

    //  ZeroG::input_handler<void> rl_cam(
    //      window, [&cam]() { cam = ZeroG::rotate_horizontally(cam, -PI /
    //      135.f);
    //      }, ZeroG::input::key::KEY_E);

    //  ZeroG::input_handler<void> gsp(
    //      window,
    //      []() { ZeroG::clock::set_game_speed(ZeroG::clock::game_speed() + 1);
    //      }, ZeroG::input::key::KEY_KP_PLUS, ZeroG::input::mod_bits::LCTRL,
    //      ZeroG::input::action::RELEASE);

    //  ZeroG::input_handler<void> gsm(
    //      window,
    //      []() { ZeroG::clock::set_game_speed(ZeroG::clock::game_speed() - 1);
    //      }, ZeroG::input::key::KEY_KP_MINUS, ZeroG::input::mod_bits::LCTRL,
    //      ZeroG::input::action::RELEASE);

    //  glm::vec2 c_pos;
    //  bool active = false;

    //  ZeroG::input_handler<ZeroG::mouse_button_data> m_b(
    //      window, [&c_pos, &active](const ZeroG::mouse_button_data &d) {
    //        if (d.button == ZeroG::input::mouse_button_bits::BUTTON_LEFT) {
    //          if (d.action == ZeroG::input::action::PRESS) {
    //            c_pos = d.mouse_position;
    //            active = true;
    //          } else if (d.action == ZeroG::input::action::RELEASE) {
    //            active = false;
    //          }
    //        }
    //      });

    //  ZeroG::input_handler<ZeroG::mouse_move_data> m_m(
    //      window,
    //      [&c_pos, &active, &window, &cam](const ZeroG::mouse_move_data &d) {
    //        if (active) {
    //          glm::vec2 offset = d.offset - c_pos;
    //          if (glm::length(offset) > 5) {
    //            window.set_cursor_pos(c_pos);
    //            offset = glm::normalize(offset);

    //            glm::vec2 a = glm::smoothstep(glm::vec2{0, 0}, c_pos,
    //            d.offset); a = a * (offset); cam = ZeroG::move_parallel(cam,
    //            -a.y); cam = ZeroG::move_perpendicular(cam, a.x);
    //          }
    //        }
    //      });

    //  ZeroG::input_handler<ZeroG::mouse_wheel_data> m_z(
    //      window, [&cam](const ZeroG::mouse_wheel_data &d) {
    //        cam = ZeroG::move_vertically(cam,
    //        ZeroG::clock::system_time_delta()
    //        /
    //                                              -10e6f * d.offset.y);
    //      });

    //  update_instancing_transforms(instanced_anim_mat2, instance_count);

    glm::mat4 PV = m.projection * m.view;

    ZeroG::uniform::set_value(e.program_id, 11, PV);
    //    for (int32_t i = 0, c = instance_count; i < c; ++i) {
    //        MVP2[i] = PV * instanced_model_mat2[i];
    //    }

    int32_t frame = 60;
    int32_t iteration = 0;
    int64_t times[10];
    while (!data_block.is_closing && !force_exit) {

      ZeroG::logger::flush();

      ZeroG::renderer::begin_frame();

      time_sum += ZeroG::clock::system_time_delta();
      m.view = ZeroG::camera_view(cam);

      update_instancing_transforms(instanced_anim_mat2, instance_count / 5);

      glm::mat4 PV = m.projection * m.view;
      ZeroG::uniform::set_value(e.program_id, 7, PV);
      for (int32_t i = 0, c = instance_count / 5; i < c; ++i) {
        model[i] = instanced_model_mat2[i] * instanced_anim_mat2[i];
      }

      ZeroG::buffer::update(&bui_model[0], 1);

      ZeroG::renderer::render_targets(&e, 1);

      ZeroG::renderer::end_frame();

      window_system.swap_buffers();

      window_system.poll();

      ZeroG::clock::update();
      if (--frame == 0) {
        frame = 60;
        times[iteration] = time_sum;
        time_sum = 0;
        ++iteration;
        if (iteration == 10)
          break;
      }
    }

    window_system.destroy_window(data_block.window_id);

    for (int32_t i = 0; i < 10; ++i) {
      ZeroG::logger::log("%f", double(times[i]) / 100000000.);
    }
  }

  ZeroG::logger::flush();
  ZeroG::logger::deinit_logger();

  ZeroG::memory::destroy(alloc);

  return 0;
}
