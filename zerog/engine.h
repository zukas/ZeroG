#ifndef ENGINE_H
#define ENGINE_H

namespace ZeroG {
class Engine {
public:
  Engine(int argc, char **argv);

  void run();

private:
};
} // namespace ZeroG

#endif // ENGINE_H
