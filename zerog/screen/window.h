#ifndef WINDOWING_H
#define WINDOWING_H

#include "common/types.h"

namespace ZeroG {

struct keyboard_data {
  input::key keycode;
  input::mod_bits modifiers;
  input::action action;
  toggle repeat;
};

struct text_data {
  char32_t codepoint;
  input::mod_bits modifiers;
};

struct mouse_move_data {
  glm::vec2 offset;
};

struct mouse_button_data {
  input::mouse_button_bits button;
  input::action action;
  input::mod_bits modifiers;
  glm::vec2 mouse_position;
};

struct mouse_wheel_data {
  glm::vec2 offset;
};

struct path_data {
  const char **paths;
  int32_t count;
};

struct key_state_data {
  const uint8_t *current_key_states;
  const uint8_t *previous_key_states;
  input::mod_bits current_mods;
  input::mod_bits previous_mods;
};

class window;

// class window_system {
// public:
//  window_system();
//  ~window_system();

//  void poll();
//  void frame_limiter(toggle state);

//  static void register_window(window *win);
//  static void unregister_window(window *win);

// private:
//  static struct window_system_data *current;
//};

typedef void (*keyboard_event)(const keyboard_data &e, void *puser);
typedef void (*text_event)(const text_data &e, void *puser);
typedef void (*mouse_move_event)(const mouse_move_data &e, void *puser);
typedef void (*mouse_button_event)(const mouse_button_data &e, void *puser);
typedef void (*mouse_wheel_event)(const mouse_wheel_data &e, void *puser);
typedef void (*path_event)(const path_data &e, void *puser);

typedef void (*key_action)(void *puser);

class window {
public:
  window();
  window(int32_t width, int32_t height, const char *title);
  window(window &&other);
  window &operator=(window &&other);
  ~window();

  window(const window &) = delete;
  window &operator=(const window &) = delete;

  void use() const;
  void swap_buffers() const;
  bool is_closing() const;

  size_t add_handler(keyboard_event callback, void *puser);

  size_t add_handler(text_event callback, void *puser);

  size_t add_handler(mouse_move_event callback, void *puser);

  size_t add_handler(mouse_button_event callback, void *puser);

  size_t add_handler(mouse_wheel_event callback, void *puser);

  size_t add_handler(path_event callback, void *puser);

  size_t track_key(key_action action, void *puser, input::key key,
                   input::mod_bits mods = input::mod_bits::NONE,
                   input::action act = input::action::PRESS);

  void remove_handler(size_t callback_id);

  void untrack_key(size_t action_id);

  bool query_key_state(input::key key, input::action action);

  void set_cursor_pos(glm::vec2 pos);

private:
  //  friend window_system;

  void handle_keyboard_event(const keyboard_data &data);

  void handle_text_event(const text_data &data);

  void handle_mouse_move_event(const mouse_move_data &data);

  void handle_mouse_button_event(const mouse_button_data &data);

  void handle_mouse_wheel_event(const mouse_wheel_data &data);

  void handle_key_actions(const key_state_data &data);

  uint32_t window_id() const;

  struct window_data_t *pdata;
};

template <typename T> class input_handler {
public:
  template <typename callable> input_handler(window &w, callable c) : rwin(w) {
    cb = malloc(sizeof(callable));
    new (cb) callable(c);
    id = rwin.add_handler(
        [](const T &data, void *puser) {
          callable *t = static_cast<callable *>(puser);
          (*t)(data);
        },
        cb);
  }

  ~input_handler() {
    rwin.remove_handler(id);
    free(cb);
  }

private:
  window &rwin;
  size_t id;
  void *cb;
};

template <> class input_handler<void> {
public:
  template <typename callable>
  input_handler(window &w, callable c, input::key key,
                input::mod_bits mods = input::mod_bits::NONE,
                input::action act = input::action::PRESS)
      : rwin(w) {
    cb = malloc(sizeof(callable));
    new (cb) callable(c);
    id = rwin.track_key(
        [](void *puser) {
          callable *t = static_cast<callable *>(puser);
          (*t)();
        },
        cb, key, mods, act);
  }

  ~input_handler() {
    rwin.untrack_key(id);
    free(cb);
  }

private:
  window &rwin;
  size_t id;
  void *cb;
};

} // namespace ZeroG

#endif // WINDOWING_H
