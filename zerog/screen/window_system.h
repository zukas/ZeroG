#ifndef WINDOW_SYSTEM_H
#define WINDOW_SYSTEM_H

#include "window_events.h"

namespace ZeroG {

namespace memory {
typedef struct allocator_t allocator_t;
}

using events_fn = void (*)(uint32_t windows_id, const event_t *events,
                           int32_t length, const key_state_t *keys,
                           void *user_data);

struct window_info_t {
  const char *window_title;
  events_fn event_handler;
  void *user_data;
  int32_t width;
  int32_t height;
};

class window_system {
public:
  window_system(memory::allocator_t *parent_allocator);
  ~window_system();

  uint32_t create_window(const window_info_t &info);
  void destroy_window(uint32_t windows_id);

  void frame_limiter(toggle state) const;

  void poll();

  void swap_buffers(uint32_t windows_id) const;

  void swap_buffers() const;

private:
  memory::allocator_t *p_parent_allocator;
  memory::allocator_t *p_allocator;
  struct internal_data_t *p_data;
};

} // namespace ZeroG

#endif // WINDOW_SYSTEM_H
