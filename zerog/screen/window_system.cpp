#include "window_system.h"

#include <cassert>
#include <cstring>

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#define SDL2_STATIC
#include <SDL.h>

#include <logger/logger.h>
#include <memory/alloc.h>

namespace ZeroG {

constexpr int32_t alloc_events{130};
constexpr int32_t max_events{128};

struct internal_data_t {
  uint32_t window_ids[4];
  struct window_data_t {
    SDL_Window *window_handle;
    SDL_GLContext context_handle;
    events_fn event_handler;
    void *user_data;
  } windows[4];
  SDL_Event sdl_events[alloc_events];
  event_t events[alloc_events];
  key_state_t key_states;
};

namespace {

int32_t find_window_idx(uint32_t *window_ids, uint32_t windows_id) {
  for (int i = 0; i < 4; ++i) {
    if (window_ids[i] == windows_id) {
      return i;
    }
  }
  ZeroG::logger::log("Cannot find window by handle: %d", windows_id);
  assert(false);
  return -1;
}

} // namespace

window_system::window_system(memory::allocator_t *parent_allocator)
    : p_parent_allocator(parent_allocator) {

  memory::stack_allocator_create_info_t create_info{
      .parent_allocator = parent_allocator,
      .size = sizeof(internal_data_t),
      .alignment = 64};

  p_allocator = memory::create(create_info);

  memory::block blk = memory::allocate(p_allocator, sizeof(internal_data_t));
  memset(blk.address, 0, sizeof(internal_data_t));
  p_data = static_cast<internal_data_t *>(blk.address);

  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    ZeroG::logger::log("SDL could not initialize! SDL Error: %s",
                       SDL_GetError());
    return;
  }
}

window_system::~window_system() {
  SDL_Quit();
  memory::destroy(p_allocator);
}

uint32_t window_system::create_window(const window_info_t &info) {

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

  SDL_Window *window = SDL_CreateWindow(
      info.window_title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      info.width, info.height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

  uint32_t window_id = SDL_GetWindowID(window);
  SDL_GLContext context = SDL_GL_CreateContext(window);
  glewExperimental = GL_TRUE;

  GLenum status = glewInit();
  if (GLEW_OK != status) {
    ZeroG::logger::log("Failed to initiase GLEW: %s",
                       glewGetErrorString(status));
    return 0;
  }
  int32_t idx = ([=]() -> int32_t {
    int32_t i;
    for (i = 0; i < 4; ++i) {
      if (p_data->window_ids[i] == 0)
        break;
    }
    return i;
  })();

  assert(idx < 4 && "Too many wndows");
  p_data->window_ids[idx] = window_id;
  p_data->windows[idx] = {.window_handle = window,
                          .context_handle = context,
                          .event_handler = info.event_handler,
                          .user_data = info.user_data};

  return window_id;
}

void window_system::destroy_window(uint32_t windows_id) {

  const int32_t idx = find_window_idx(p_data->window_ids, windows_id);
  p_data->window_ids[idx] = 0;
  SDL_GL_DeleteContext(p_data->windows[idx].context_handle);
  SDL_DestroyWindow(p_data->windows[idx].window_handle);
  memset(&p_data->windows[idx], 0, sizeof(internal_data_t::window_data_t));
}

void window_system::frame_limiter(toggle state) const {
  SDL_GL_SetSwapInterval(value(state));
}

void window_system::poll() {

  SDL_PumpEvents();
  SDL_Event *sdl_events = p_data->sdl_events;
  event_t *events = p_data->events;
  __builtin_prefetch(sdl_events);
  __builtin_prefetch(events);

  memset(sdl_events, 0, max_events * sizeof(SDL_Event));
  memset(events, 0, max_events * sizeof(event_t));

  const int32_t count = SDL_PeepEvents(sdl_events, max_events, SDL_GETEVENT,
                                       SDL_FIRSTEVENT, SDL_LASTEVENT);
  const uint32_t current_window = SDL_GetWindowID(SDL_GL_GetCurrentWindow());

  const int32_t idx = find_window_idx(p_data->window_ids, current_window);

  const internal_data_t::window_data_t &window = p_data->windows[idx];
  int32_t out_idx = 0;
  for (int32_t i = 0; i < count; ++i) {
    const SDL_Event &e = sdl_events[i];
    switch (e.type) {
    case SDL_WINDOWEVENT: {
      if (e.window.windowID != current_window) {
        logger::log("Window event %u, but current window %u", e.window.windowID,
                    current_window);
        break;
      }

      switch (e.window.event) {
      case SDL_WINDOWEVENT_SHOWN:
        logger::log("SDL_WINDOWEVENT_SHOWN %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_SHOWN,
                             .shown = window_shown_t{}};
        break;
      case SDL_WINDOWEVENT_HIDDEN:
        logger::log("SDL_WINDOWEVENT_HIDDEN %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_HIDDEN,
                             .hidden = window_hidden_t{}};
        break;
      case SDL_WINDOWEVENT_EXPOSED:
        logger::log("SDL_WINDOWEVENT_EXPOSED %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_EXPOSED,
                             .exposed = window_exposed_t{}};
        break;
      case SDL_WINDOWEVENT_MOVED:
        logger::log("SDL_WINDOWEVENT_MOVED %u", current_window);
        events[out_idx++] = {
            .type = event_type_t::WINDOW_MOVED,
            .moved = window_moved_t{e.window.data1, e.window.data2}};
        break;
      case SDL_WINDOWEVENT_RESIZED:
        logger::log("SDL_WINDOWEVENT_RESIZED %u", current_window);
        events[out_idx++] = {
            .type = event_type_t::WINDOW_RESIZED,
            .resized = window_resized_t{e.window.data1, e.window.data2}};
        break;
      case SDL_WINDOWEVENT_SIZE_CHANGED:
        logger::log("SDL_WINDOWEVENT_SIZE_CHANGED %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_SIZE_CHANGED,
                             .size_changed = window_size_changed_t{
                                 e.window.data1, e.window.data2}};
        break;
      case SDL_WINDOWEVENT_MINIMIZED:
        logger::log("SDL_WINDOWEVENT_MINIMIZED %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_MINIMIZED,
                             .minimized = window_minimized_t{}};
        break;
      case SDL_WINDOWEVENT_MAXIMIZED:
        logger::log("SDL_WINDOWEVENT_MAXIMIZED %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_MAXIMIZED,
                             .maximized = window_maximized_t{}};
        break;
      case SDL_WINDOWEVENT_RESTORED:
        logger::log("SDL_WINDOWEVENT_RESTORED %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_RESTORED,
                             .restored = window_restored_t{}};
        break;
      case SDL_WINDOWEVENT_ENTER:
        logger::log("SDL_WINDOWEVENT_ENTER %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_ENTER,
                             .mouse_entered = window_mouse_entered_t{}};
        break;
      case SDL_WINDOWEVENT_LEAVE:
        logger::log("SDL_WINDOWEVENT_LEAVE %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_LEAVE,
                             .mouse_leave = window_mouse_leave_t{}};
        break;
      case SDL_WINDOWEVENT_FOCUS_GAINED:
        logger::log("SDL_WINDOWEVENT_FOCUS_GAINED %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_FOCUS_GAINED,
                             .focus_gained = window_focus_gained_t{}};
        break;
      case SDL_WINDOWEVENT_FOCUS_LOST:
        logger::log("SDL_WINDOWEVENT_FOCUS_LOST %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_FOCUS_LOST,
                             .focus_lost = window_focus_lost_t{}};
        break;
      case SDL_WINDOWEVENT_CLOSE:
        logger::log("SDL_WINDOWEVENT_CLOSE %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_CLOSE,
                             .close = window_close_t{}};
        break;
      case SDL_WINDOWEVENT_TAKE_FOCUS:
        logger::log("SDL_WINDOWEVENT_TAKE_FOCUS %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_TAKE_FOCUS,
                             .take_focus = window_take_focus_t{}};
        break;
      case SDL_WINDOWEVENT_HIT_TEST:
        logger::log("SDL_WINDOWEVENT_HIT_TEST %u", current_window);
        events[out_idx++] = {.type = event_type_t::WINDOW_HIT_TEST,
                             .hit_test = window_hit_test_t{}};
        break;
      default:
        ZeroG::logger::log("Window %d got unknown event %d", e.window.windowID,
                           e.window.event);
        break;
      }
      break;
    }
    case SDL_KEYDOWN:
    case SDL_KEYUP: {
      if (e.window.windowID != current_window)
        break;
      logger::log("SDL_KEY %u", current_window);
      events[out_idx++] = {
          .type = event_type_t::WINDOW_KEY,
          .key = window_key_t{static_cast<input::key>(e.key.keysym.scancode),
                              static_cast<input::mod_bits>(e.key.keysym.mod),
                              static_cast<input::action>(e.key.state),
                              static_cast<toggle>(e.key.repeat)}};
      break;
    }
    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP: {
      if (e.window.windowID != current_window)
        break;
      logger::log("SDL_MOUSEBUTTON %u", current_window);
      events[out_idx++] = {
          .type = event_type_t::WINDOW_MOUSE_BUTTON,
          .mouse_button = window_mouse_button_t{
              static_cast<input::mouse_button_bits>(e.button.button),
              static_cast<input::action>(e.button.state),
              static_cast<input::mod_bits>(SDL_GetModState()),
              static_cast<input::mouse_click_count>(e.button.clicks),
              e.button.x, e.button.y}};
      break;
    }
    case SDL_MOUSEMOTION: {
      if (e.window.windowID != current_window)
        break;
      logger::log("SDL_MOUSEMOTION %u", current_window);
      events[out_idx++] = {
          .type = event_type_t::WINDOW_MOUSE_MOVE,
          .mouse_move = window_mouse_move_t{
              e.motion.x, e.motion.y, e.motion.xrel, e.motion.yrel,
              static_cast<input::mouse_button_bits>(e.motion.state)}};
      break;
    }
    case SDL_MOUSEWHEEL: {
      if (e.window.windowID != current_window)
        break;
      logger::log("SDL_MOUSEWHEEL %u", current_window);
      events[out_idx++] = {
          .type = event_type_t::WINDOW_MOUSE_WHEEL,
          .mouse_wheel = window_mouse_wheel_t{
              e.wheel.x, e.wheel.y,
              static_cast<input::mouse_wheel_direction>(e.wheel.direction)}};
      break;
    }
    case SDL_TEXTEDITING: {
      if (e.window.windowID != current_window)
        break;
      logger::log("SDL_TEXTEDITING %u", current_window);
      events[out_idx++] = {.type = event_type_t::WINDOW_TEXT_EDIT,
                           .text_edit = window_text_edit_t{
                               e.edit.text, e.edit.start, e.edit.length}};
      break;
    }
    }
  }

  events[out_idx++] = {.type = event_type_t::END_STREAM};

  p_data->key_states.previous_mods = p_data->key_states.current_mods;
  memcpy(p_data->key_states.previous_key_states,
         p_data->key_states.current_key_states,
         sizeof(p_data->key_states.current_key_states));
  p_data->key_states.current_mods =
      static_cast<input::mod_bits>(SDL_GetModState());
  memcpy(p_data->key_states.current_key_states, SDL_GetKeyboardState(nullptr),
         sizeof(p_data->key_states.current_key_states));
  window.event_handler(current_window, events, out_idx, &p_data->key_states,
                       window.user_data);
}

void window_system::swap_buffers(uint32_t windows_id) const {

  SDL_GL_SwapWindow(
      p_data->windows[find_window_idx(p_data->window_ids, windows_id)]
          .window_handle);
}

void window_system::swap_buffers() const {
  for (int32_t i = 0; i < 4; ++i) {
    if (p_data->windows[i].window_handle) {
      SDL_GL_SwapWindow(p_data->windows[i].window_handle);
    }
  }
}

} // namespace ZeroG
