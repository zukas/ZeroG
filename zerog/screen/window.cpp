#include "window.h"

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#define SDL2_STATIC
#include <SDL.h>

#include <cassert>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <functional>
#include <unordered_map>
#include <vector>

#include <logger/logger.h>

namespace ZeroG {

#define t_S1(x) #x
#define t_S2(x) t_S1(x)
#define MSG "Calling '" t_S2(__FUNCTION__) "' on an empty window structure"

// struct window_system_data {
//  window_system *self{nullptr};
//  std::unordered_map<uint32_t, window *> windows;
//  uint32_t active_window_id{0};
//  uint8_t prev_key_states[SDL_NUM_SCANCODES]{0};
//  input::mod_bits prev_mods;
//} *window_system::current = nullptr;

// static uint8_t windows_system_store[sizeof(window_system_data)];

// window_system::window_system() {

//  assert(current == nullptr && "windows system already initialized");

//  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
//    ZeroG::logger::log("SDL could not initialize! SDL Error: %s\n",
//                       SDL_GetError());
//    return;
//  }

//  current = new (windows_system_store)
//      window_system_data{this, {}, 0, {}, input::mod_bits::NONE};
//}

// window_system::~window_system() {
//  assert(current && "No windows system context");
//  current = nullptr;
//  SDL_Quit();
//}

// void window_system::poll() {
//  assert(current && "No windows system context");

//  SDL_Event e;
//  while (SDL_PollEvent(&e)) {
//    switch (e.type) {
//    case SDL_WINDOWEVENT: {
//      switch (e.window.event) {
//      case SDL_WINDOWEVENT_SHOWN:
//        ZeroG::logger::log("Window %d shown", e.window.windowID);
//        break;
//      case SDL_WINDOWEVENT_HIDDEN:
//        ZeroG::logger::log("Window %d hidden", e.window.windowID);
//        break;
//      case SDL_WINDOWEVENT_EXPOSED:
//        ZeroG::logger::log("Window %d exposed", e.window.windowID);
//        break;
//      case SDL_WINDOWEVENT_MOVED:
//        ZeroG::logger::log("Window %d moved to %d,%d", e.window.windowID,
//                           e.window.data1, e.window.data2);
//        break;
//      case SDL_WINDOWEVENT_RESIZED:
//        ZeroG::logger::log("Window %d resized to %dx%d", e.window.windowID,
//                           e.window.data1, e.window.data2);
//        break;
//      case SDL_WINDOWEVENT_SIZE_CHANGED:
//        ZeroG::logger::log("Window %d size changed to %dx%d",
//        e.window.windowID,
//                           e.window.data1, e.window.data2);
//        break;
//      case SDL_WINDOWEVENT_MINIMIZED:
//        ZeroG::logger::log("Window %d minimized", e.window.windowID);
//        break;
//      case SDL_WINDOWEVENT_MAXIMIZED:
//        ZeroG::logger::log("Window %d maximized", e.window.windowID);
//        break;
//      case SDL_WINDOWEVENT_RESTORED:
//        ZeroG::logger::log("Window %d restored", e.window.windowID);
//        break;
//      case SDL_WINDOWEVENT_ENTER:
//        ZeroG::logger::log("Mouse entered window %d", e.window.windowID);
//        break;
//      case SDL_WINDOWEVENT_LEAVE:
//        ZeroG::logger::log("Mouse left window %d", e.window.windowID);
//        break;
//      case SDL_WINDOWEVENT_FOCUS_GAINED:
//        ZeroG::logger::log("Window %d gained keyboard focus",
//                           e.window.windowID);
//        current->active_window_id = e.window.windowID;
//        break;
//      case SDL_WINDOWEVENT_FOCUS_LOST:
//        ZeroG::logger::log("Window %d lost keyboard focus",
//        e.window.windowID); if (current->active_window_id ==
//        e.window.windowID) {
//          current->active_window_id = 0;
//        }
//        break;
//      case SDL_WINDOWEVENT_CLOSE:
//        ZeroG::logger::log("Window %d closed", e.window.windowID);
//        break;
//      case SDL_WINDOWEVENT_TAKE_FOCUS:
//        ZeroG::logger::log("Window %d is offered a focus", e.window.windowID);
//        break;
//      case SDL_WINDOWEVENT_HIT_TEST:
//        ZeroG::logger::log("Window %d has a special hit test",
//                           e.window.windowID);
//        break;
//      default:
//        ZeroG::logger::log("Window %d got unknown event %d",
//        e.window.windowID,
//                           e.window.event);
//        break;
//      }
//      break;
//    }
//    case SDL_KEYDOWN:
//    case SDL_KEYUP: {
//      auto win = current->windows[e.key.windowID];
//      if (win) {
//        win->handle_keyboard_event(
//            {static_cast<input::key>(e.key.keysym.scancode),
//             static_cast<input::mod_bits>(e.key.keysym.mod),
//             static_cast<input::action>(e.key.state),
//             static_cast<toggle>(e.key.repeat)});
//      }
//      break;
//    }
//    case SDL_MOUSEBUTTONDOWN:
//    case SDL_MOUSEBUTTONUP: {
//      auto win = current->windows[e.key.windowID];
//      if (win) {

//        win->handle_mouse_button_event(
//            {static_cast<input::mouse_button_bits>(e.button.button),
//             static_cast<input::action>(e.button.state),
//             static_cast<input::mod_bits>(SDL_GetModState()),
//             {e.button.x, e.button.y}});
//      }

//      break;
//    }
//    case SDL_MOUSEMOTION: {
//      auto win = current->windows[e.key.windowID];
//      if (win) {
//        win->handle_mouse_move_event({{e.motion.x, e.motion.y}});
//      }
//      break;
//    }
//    case SDL_MOUSEWHEEL: {
//      auto win = current->windows[e.key.windowID];
//      if (win) {
//        win->handle_mouse_wheel_event({{e.wheel.x, e.wheel.y}});
//      }
//      break;
//    }
//    }
//  }

//  const uint8_t *states = SDL_GetKeyboardState(nullptr);
//  const input::mod_bits mods =
//  static_cast<input::mod_bits>(SDL_GetModState());

//  auto win = current->windows[current->active_window_id];
//  if (win) {
//    win->handle_key_actions(
//        {states, current->prev_key_states, mods, current->prev_mods});
//  }

//  std::copy(states, &states[SDL_NUM_SCANCODES], current->prev_key_states);
//  current->prev_mods = mods;
//}

// void window_system::frame_limiter(toggle state) {
//  SDL_GL_SetSwapInterval(value(state));
//}

// void window_system::register_window(window *win) {
//  assert(current && "No windows system context");
//  current->windows[win->window_id()] = win;
//}

// void window_system::unregister_window(window *win) {
//  assert(current && "No windows system context");
//  current->windows.erase(win->window_id());
//}

struct key_action_handler {
  input::key key;
  input::mod_bits mods;
  input::action action;
  void *puser{nullptr};
  key_action callback{nullptr};
};

struct keyboard_handler {
  void *puser{nullptr};
  keyboard_event callback{nullptr};
};

struct text_handler {
  void *puser{nullptr};
  text_event callback{nullptr};
};

struct mouse_move_handler {
  void *puser{nullptr};
  mouse_move_event callback{nullptr};
};

struct mouse_button_handler {
  void *puser{nullptr};
  mouse_button_event callback{nullptr};
};

struct mouse_wheel_handler {
  void *puser{nullptr};
  mouse_wheel_event callback{nullptr};
};

struct path_handler {
  void *puser{nullptr};
  path_event callback{nullptr};
};

struct window_data_t {
  SDL_Window *instance{nullptr};
  uint32_t window_id{0};
  uint32_t padding1;
  SDL_GLContext context{nullptr};
  std::vector<key_action_handler> key_action_handlers;
  std::vector<keyboard_handler> keyboard_handlers;
  std::vector<text_handler> text_handlers;
  std::vector<mouse_move_handler> mouse_pos_handlers;
  std::vector<mouse_button_handler> mouse_button_handlers;
  std::vector<mouse_wheel_handler> mouse_wheel_handlers;
  std::vector<path_handler> path_handlers;
};

typedef window_data_t *window_data;

window::window() : pdata(nullptr) {}

window::window(int32_t width, int32_t height, const char *title)
    : pdata(new window_data_t) {
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

  pdata->instance =
      SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                       width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

  pdata->window_id = SDL_GetWindowID(pdata->instance);
  pdata->context = SDL_GL_CreateContext(pdata->instance);
  glewExperimental = GL_TRUE;

  glewInit();
  //  window_system::register_window(this);
} // namespace ZeroG

window::window(window &&other) {
  window_data tmp = other.pdata;
  other.pdata = pdata;
  pdata = tmp;
  //  window_system::register_window(this);
}

window &window::operator=(window &&other) {
  window_data tmp = other.pdata;
  other.pdata = pdata;
  pdata = tmp;
  return *this;
}

window::~window() {
  //  window_system::unregister_window(this);
  if (pdata && pdata->instance) {
    SDL_DestroyWindow(pdata->instance);
  }
  delete pdata;
  pdata = nullptr;
}

void window::use() const {
  assert(pdata && pdata->instance && MSG);
  SDL_GL_MakeCurrent(pdata->instance, pdata->context);
}

void window::swap_buffers() const {
  assert(pdata && pdata->instance && MSG);
  SDL_GL_SwapWindow(pdata->instance);
}

bool window::is_closing() const {
  assert(pdata && pdata->instance && MSG);
  return false;
}

template <typename T> size_t hash(T cb, void *p) {
  return std::hash<T>{}(cb) + std::hash<void *>{}(p);
}

template <typename T>
void generic_remove_callback(std::vector<T> &vec, size_t cid) {
  const auto end = std::end(vec);
  const auto it = std::find_if(std::begin(vec), end, [cid](const auto &d) {
    return std::hash<decltype(d.callback)>{}(d.callback) == cid;
  });
  if (it != end) {
    vec.erase(it);
  }
}

size_t window::add_handler(keyboard_event callback, void *puser) {
  assert(pdata && pdata->instance && MSG);
  pdata->keyboard_handlers.push_back({puser, callback});
  return hash(callback, puser);
}

size_t window::add_handler(text_event callback, void *puser) {
  assert(pdata && pdata->instance && MSG);
  pdata->text_handlers.push_back({puser, callback});
  return hash(callback, puser);
}

size_t window::add_handler(mouse_move_event callback, void *puser) {
  assert(pdata && pdata->instance && MSG);
  pdata->mouse_pos_handlers.push_back({puser, callback});
  return hash(callback, puser);
}

size_t window::add_handler(mouse_button_event callback, void *puser) {
  assert(pdata && pdata->instance && MSG);
  pdata->mouse_button_handlers.push_back({puser, callback});
  return hash(callback, puser);
}

size_t window::add_handler(mouse_wheel_event callback, void *puser) {
  assert(pdata && pdata->instance && MSG);
  pdata->mouse_wheel_handlers.push_back({puser, callback});
  return hash(callback, puser);
}

size_t window::add_handler(path_event callback, void *puser) {
  assert(pdata && pdata->instance && MSG);
  pdata->path_handlers.push_back({puser, callback});
  return hash(callback, puser);
}

size_t window::track_key(key_action action, void *puser, input::key key,
                         input::mod_bits mods, input::action act) {
  pdata->key_action_handlers.push_back({key, mods, act, puser, action});
  return hash(action, puser);
}

void window::remove_handler(size_t callback_id) {
  assert(pdata && pdata->instance && MSG);
  generic_remove_callback(pdata->keyboard_handlers, callback_id);
  generic_remove_callback(pdata->text_handlers, callback_id);
  generic_remove_callback(pdata->mouse_pos_handlers, callback_id);
  generic_remove_callback(pdata->mouse_button_handlers, callback_id);
  generic_remove_callback(pdata->mouse_wheel_handlers, callback_id);
  generic_remove_callback(pdata->path_handlers, callback_id);
}

void window::untrack_key(size_t action_id) {
  assert(pdata && pdata->instance && MSG);
  generic_remove_callback(pdata->key_action_handlers, action_id);
}

bool window::query_key_state(input::key key, input::action action) {
  assert(pdata && pdata->instance && MSG);
  const uint8_t *states = SDL_GetKeyboardState(nullptr);
  return input::action(states[value(key)]) == action;
}

void window::set_cursor_pos(glm::vec2 pos) {
  assert(pdata && pdata->instance && MSG);
  SDL_WarpMouseInWindow(pdata->instance, static_cast<int32_t>(pos.x),
                        static_cast<int32_t>(pos.y));
}

void window::handle_keyboard_event(const keyboard_data &data) {
  assert(pdata && pdata->instance && MSG);
  for (auto &h : pdata->keyboard_handlers) {
    h.callback(data, h.puser);
  }
}

void window::handle_text_event(const text_data &data) {}

void window::handle_mouse_move_event(const mouse_move_data &data) {
  assert(pdata && pdata->instance && MSG);
  for (auto &h : pdata->mouse_pos_handlers) {
    h.callback(data, h.puser);
  }
}

void window::handle_mouse_button_event(const mouse_button_data &data) {
  assert(pdata && pdata->instance && MSG);
  for (auto &h : pdata->mouse_button_handlers) {
    h.callback(data, h.puser);
  }
}

void window::handle_mouse_wheel_event(const mouse_wheel_data &data) {
  assert(pdata && pdata->instance && MSG);
  for (auto &h : pdata->mouse_wheel_handlers) {
    h.callback(data, h.puser);
  }
}

void window::handle_key_actions(const key_state_data &data) {

  for (auto &h : pdata->key_action_handlers) {
    if (h.action == input::action::RELEASE &&
        (input::action(data.previous_key_states[value(h.key)]) !=
             input::action::PRESS ||
         (data.previous_mods & h.mods) != h.mods)) {
      continue;
    }

    if (input::action(data.current_key_states[value(h.key)]) == h.action &&
        (data.current_mods & h.mods) == h.mods) {
      h.callback(h.puser);
    }
  }
}

uint32_t window::window_id() const {
  assert(pdata && pdata->instance && MSG);
  return pdata->window_id;
}

// namespace win

} // namespace ZeroG
