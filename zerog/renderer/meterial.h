#ifndef METERIAL_H
#define METERIAL_H

#include <common/zerog_def.h>
#include <glm/glm.hpp>

namespace ZeroG {

namespace memory {
typedef struct allocator_t allocator_t;
}

namespace renderer {

struct colour_material_constant_data_t {
  glm::vec4 diffuse_colour;
  glm::vec4 specular_colour;
  float ambient_intensity;
  float diffuse_intensity;
  float specular_intensity;
  float diffuse_mix_ratio;
  float spacular_mix_ratio;
};

struct material_system_definition_t {
  memory::allocator_t *parent_alloc;
  int32_t max_material_count;
};

void init_material_system(const material_system_definition_t &init_info);

void deinit_material_system();

void bind_materials(uint32_t binding_point);

void unbind_materials(uint32_t binding_point);

int32_t create_material(const char *name,
                        const colour_material_constant_data_t &material);

void delete_material(int32_t maerial_index);

void delete_material(const char *name);

colour_material_constant_data_t get_material(int32_t material_index);

colour_material_constant_data_t get_material(const char *name);

void update_material(int32_t maerial_index,
                     const colour_material_constant_data_t &material);

void update_material(const char *name,
                     const colour_material_constant_data_t &material);

} // namespace renderer
} // namespace ZeroG

#endif // METERIAL_H
