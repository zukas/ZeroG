#ifndef SCENE_DATA_H
#define SCENE_DATA_H

#include <common/zerog_def.h>
#include <glm/glm.hpp>

namespace ZeroG {

namespace memory {
typedef struct allocator_t allocator_t;
}

namespace renderer {

struct scene_date_definition_t {};

void init_scene_data(const scene_date_definition_t &init_info);

void deinit_scene_data();

void bind_3d_scene_data(uint32_t binding_point);

void unbind_3d_scene_data(uint32_t binding_point);

void bind_2d_scene_data(uint32_t binding_point);

void unbind_2d_scene_data(uint32_t binding_point);

void update_3d_camera_data();

void update_2d_camera_data();

void update_3d_environment_data();

} // namespace renderer
} // namespace ZeroG
#endif // SCENE_DATA_H
