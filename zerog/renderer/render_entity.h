#ifndef RENDER_ENTITY_H
#define RENDER_ENTITY_H

#include <common/zerog_def.h>
#include <glm/glm.hpp>

#include "data_types.h"

namespace ZeroG {

namespace memory {
typedef struct allocator_t allocator_t;
}

namespace renderer {

struct render_entity_definition_t {
  buffer_create_info_t vertex_buffer_info;
  buffer_create_info_t index_buffer_info;
  buffer_create_info_t instanced_buffer_info;

  const vertex_buffer_section_definition_t *vertex_buffer_definition;
  const vertex_buffer_section_definition_t *instanced_buffer_definition;

  const shader_source_t *shaders;
  const shader_uniform_definition_t *shader_uniforms;

  int32_t shader_count;
  int32_t shader_uniform_count;
};

typedef struct render_entity_t render_entity_t;

} // namespace renderer
} // namespace ZeroG

#endif // RENDER_ENTITY_H
