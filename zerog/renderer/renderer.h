#ifndef RENDERER_H
#define RENDERER_H

#include "common/zerog_def.h"

namespace ZeroG {

struct entity;

namespace renderer {

void begin_frame();
void end_frame();

void render_targets(const entity *entities, int32_t count);

} // namespace renderer
} // namespace ZeroG

#endif // RENDERER_H
