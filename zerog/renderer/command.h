#ifndef COMMAND_H
#define COMMAND_H

#include <common/zerog_def.h>
#include <glm/glm.hpp>

namespace ZeroG {

namespace memory {
typedef struct allocator_t allocator_t;
}

namespace renderer {

struct command_pool_definition_t {};

typedef struct command_pool_t command_pool_t;
typedef struct command_buffer_t command_buffer_t;

command_pool_t *create_command_pool(const command_pool_definition_t &def);

void destroy_command_pool(const command_pool_t *pool);

command_buffer_t create_command_buffer(command_pool_t *pool);

} // namespace renderer
} // namespace ZeroG

#endif // COMMAND_H
