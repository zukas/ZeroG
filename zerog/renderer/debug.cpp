#include "debug.h"

#include <GL/glew.h>

#include <logger/logger.h>

void GLAPIENTRY bebug_callback(GLenum source, GLenum type, GLuint id,
                               GLenum severity, GLsizei length,
                               const GLchar *message, const void *userParam) {
  ZeroG::logger::log("[GLLOG] %s type = 0x%x, severity = 0x%x, message = %s",
                     (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
                     type, severity, message);
}

void ZeroG::debug_init() {
  glEnable(GL_DEBUG_OUTPUT);
  glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
  glDebugMessageCallback(bebug_callback, nullptr);
}
