#ifndef MESH_H
#define MESH_H

#include "data_types.h"
#include <common/types.h>

namespace ZeroG {

struct mesh_t {
  uint32_t m_vao{0};
  uint32_t m_buffers[2]{0};
  uint32_t m_elem_count{0};
};

} // namespace ZeroG

#endif // MESH_H
