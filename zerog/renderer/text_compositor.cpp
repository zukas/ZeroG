#include "text_compositor.h"

#include <common/math.h>
#include <memory/alloc.h>

#include <ft2build.h>
#include FT_FREETYPE_H

namespace ZeroG {

namespace renderer {

struct text_compositor_t {

  uint32_t texture_id;
  struct {
    glm::vec2 advance;
    glm::vec2 bitmap_dimentions;
    glm::vec2 bitmap_position;
    glm::vec2 glyph_offset;
  } char_info[128];

  uint32_t texture_width;
  uint32_t texture_height;

  memory::allocator_t *allocator;
};

constexpr const char *text_vs =
    R"(
        #version 450 core

        layout(location = 0) in vec2 attr_position;
        layout(location = 0) in vec2 attr_uv;

        out vec2 fs_in_uv;

        void main()
        {
            gl_Position = vec4(attr_position, 0, 1);
            fs_in_uv = attr_uv;
        }
    )";

constexpr const char *text_fs =
    R"(
        in vec2 fs_in_uv;

        uniform sampler2D glyph_texture;
        uniform vec4 text_colour;

        out vec4 out_frag_colour;

        void main()
        {
            out_frag_colour = vec4(1, 1, 1, texture2D(glyph_texture, fs_in_uv).a) * text_colour;
        }
    )";

struct {
  memory::allocator_t *alloc;
  text_compositor_t *compositors;
  uint32_t compositor_data_buffer_id;
  uint32_t compositor_shader_program_id;
  struct {
    uint32_t glyph_texture;
    uint32_t text_colour;
  } compositor_uniforms;
  int64_t compositor_block_size;
  int32_t count;
} __text_compositor_store{nullptr, nullptr, 0, 0, {0, 0}, 0, 0};

void init_text_compositor(const text_compositor_init_t &info) {}

void deinit_text_compositor() {

  __text_compositor_store = {nullptr, nullptr, 0, 0, {0, 0}, 0, 0};
}

int32_t
create_text_compositor(const text_compositor_definition_t &compositor_info) {

  FT_Library ft_lib{nullptr};
  FT_Init_FreeType(&ft_lib);

  FT_Face face{nullptr};
  FT_New_Face(ft_lib, compositor_info.font_file, 0, &face);

  FT_Done_FreeType(ft_lib);
}

void destroy_text_compositor(int32_t compsitor) {}

void render_text(int32_t compositor, const char *text,
                 const text_bounding_box &bounding_box) {}

} // namespace renderer

} // namespace ZeroG
