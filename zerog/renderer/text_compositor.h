#ifndef TEXT_COMPOSITOR_H
#define TEXT_COMPOSITOR_H

#include <common/zerog_def.h>
#include <glm/glm.hpp>

namespace ZeroG {

namespace memory {
typedef struct allocator_t allocator_t;
}

namespace renderer {

struct text_compositor_init_t {
  memory::allocator_t *parent_alloc;
  int32_t max_compositor_count;
};

struct text_compositor_definition_t {
  const char *font_file;
  int32_t font_size;
};

struct text_bounding_box {
  glm::vec2 position;
  glm::vec2 dimentions;
};

void init_text_compositor(const text_compositor_init_t &info);

void deinit_text_compositor();

int32_t
create_text_compositor(const text_compositor_definition_t &compositor_info);

void destroy_text_compositor(int32_t compsitor);

void render_text(int32_t compositor, const char *text,
                 const text_bounding_box &bounding_box);

} // namespace renderer
} // namespace ZeroG
#endif // TEXT_COMPOSITOR_H
