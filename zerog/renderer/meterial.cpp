#include "meterial.h"

#include "utils.h"

#include <common/hash.h>
#include <logger/logger.h>
#include <memory/alloc.h>

#include <cassert>
#include <string.h>

namespace ZeroG {

namespace renderer {

struct material_name_t {
  uint64_t name_hash;
};

static struct {
  material_name_t *names;
  colour_material_constant_data_t *data;

  memory::allocator_t *alloc;
  int32_t max_material_count;
  uint32_t uniform_block_id;
  int64_t name_alloc_size;
  int64_t data_alloc_size;
} __material_store{
    nullptr, nullptr, nullptr, 0, 0, 0, 0,
};

static constexpr int64_t alignment = 16;

static int32_t get_index(const char *name) {
  uint64_t hash = common::hash(name);
  int32_t count = __material_store.max_material_count;
  material_name_t *names = __material_store.names;
  for (int32_t i = 0; i < count; ++i) {
    if (names[i].name_hash == hash) {
      return i;
    }
  }
  return -1;
}

void init_material_system(const material_system_definition_t &init_info) {

  int64_t min_req_name_size = memory::align_64(
      size_of<material_name_t, int64_t>() * init_info.max_material_count,
      alignment);
  int64_t min_req_data_size =
      memory::align_64(size_of<colour_material_constant_data_t, int64_t>() *
                           init_info.max_material_count,
                       alignment);

  memory::stack_allocator_create_info_t alloc_info;
  alloc_info.parent_allocator = init_info.parent_alloc;
  alloc_info.size = min_req_name_size + min_req_data_size;
  alloc_info.alignment = alignment;

  __material_store.alloc = memory::create(alloc_info);
  __material_store.max_material_count = init_info.max_material_count;

  memory::block name_blk =
      memory::allocate(__material_store.alloc, min_req_name_size);
  memory::block data_blk =
      memory::allocate(__material_store.alloc, min_req_name_size);

  __material_store.names = static_cast<material_name_t *>(name_blk.address);
  __material_store.name_alloc_size = name_blk.size;

  __material_store.data =
      static_cast<colour_material_constant_data_t *>(data_blk.address);
  __material_store.data_alloc_size = data_blk.size;

  memset(__material_store.names, 0, static_cast<size_t>(min_req_name_size));
  memset(__material_store.data, 0, static_cast<size_t>(min_req_data_size));

  buffer_create_info_t buffer_info;
  buffer_info.type = buffer_type_e::STATIC;
  buffer_info.size = static_cast<uint32_t>(min_req_data_size);
  buffer_info.data = __material_store.data;

  buffer::create(&buffer_info, 1, &__material_store.uniform_block_id);
}

void deinit_material_system() {

  buffer::destroy(&__material_store.uniform_block_id, 1);

  memory::deallocate_all(__material_store.alloc);
  memory::destroy(__material_store.alloc);

  __material_store = {
      nullptr, nullptr, nullptr, 0, 0, 0, 0,
  };
}

void bind_materials(uint32_t binding_point) {
  uniform::bind_buffer(binding_point, __material_store.uniform_block_id);
}

void unbind_materials(uint32_t binding_point) {
  uniform::bind_buffer(binding_point, 0);
}

int32_t create_material(const char *name,
                        const colour_material_constant_data_t &material) {
  uint64_t hash = common::hash(name);
  int32_t count = __material_store.max_material_count;
  material_name_t *names = __material_store.names;
  for (int32_t i = 0; i < count; ++i) {
    if (names[i].name_hash == 0) {
      names[i].name_hash = hash;
      __material_store.data[i] = material;

      return i;
    }
  }
  return -1;
}

void delete_material(int32_t material_index) {
  assert(material_index >= 0 && "Invalid material handle");
  assert(material_index < __material_store.max_material_count &&
         "Material index is out of bounds");
  __material_store.names[material_index].name_hash = 0;
}

void delete_material(const char *name) { delete_material(get_index(name)); }

colour_material_constant_data_t get_material(int32_t material_index) {
  assert(material_index >= 0 && "Invalid material handle");
  assert(material_index < __material_store.max_material_count &&
         "Material index is out of bounds");
  return __material_store.data[material_index];
}

colour_material_constant_data_t get_material(const char *name) {
  return get_material(get_index(name));
}

void update_material(int32_t material_index,
                     const colour_material_constant_data_t &material) {
  assert(material_index >= 0 && "Invalid material handle");
  assert(material_index < __material_store.max_material_count &&
         "Material index is out of bounds");
  __material_store.data[material_index] = material;
}

void update_material(const char *name,
                     const colour_material_constant_data_t &material) {
  update_material(get_index(name), material);
}

} // namespace renderer

} // namespace ZeroG
