#include "tst_atomic_pool_allocator.h"
#include "tst_bitmapped_allocator.h"
#include "tst_pool_allocator.h"
#include "tst_section_allocator.h"
#include "tst_stack_allocator.h"

#include <gtest/gtest.h>

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
