#ifndef TST_CREATE_STACK_ALLOCATOR_H
#define TST_CREATE_STACK_ALLOCATOR_H

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include <memory/alloc.h>

using namespace testing;

TEST(stack_allocator, create) {

  using namespace ZeroG::memory;

  stack_allocator_create_info_t info;
  info.size = 1024 * 1024;
  info.alignment = 32;
  info.parent_allocator = nullptr;

  allocator_t *alloc = create(info);

  EXPECT_NE(alloc, nullptr);

  const uint64_t aligned_size = preferred_size(alloc, 32);

  EXPECT_EQ(aligned_size, 32);

  destroy(alloc);
}

TEST(stack_allocator, allocate1) {

  using namespace ZeroG::memory;

  stack_allocator_create_info_t info;
  info.size = 1024 * 1024;
  info.alignment = 32;
  info.parent_allocator = nullptr;

  allocator_t *alloc = create(info);

  EXPECT_NE(alloc, nullptr);

  const block mem_blk = allocate(alloc, 32);

  EXPECT_EQ(mem_blk.size, 32);

  EXPECT_EQ((reinterpret_cast<uintptr_t>(mem_blk.address) & 31), 0);

  deallocate(alloc, mem_blk);

  destroy(alloc);
}

TEST(stack_allocator, allocate2) {

  using namespace ZeroG::memory;

  stack_allocator_create_info_t info;
  info.size = 1024 * 1024;
  info.alignment = 32;
  info.parent_allocator = nullptr;

  allocator_t *alloc = create(info);

  EXPECT_NE(alloc, nullptr);

  const block mem_blk = allocate(alloc, 17);

  EXPECT_EQ(mem_blk.size, 32);

  EXPECT_EQ((reinterpret_cast<uintptr_t>(mem_blk.address) & 31), 0);

  deallocate(alloc, mem_blk);

  destroy(alloc);
}

#endif // TST_CREATE_STACK_ALLOCATOR_H
