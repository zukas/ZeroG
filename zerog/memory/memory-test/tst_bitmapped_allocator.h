#ifndef TST_BITMAPPED_ALLOCATOR_H
#define TST_BITMAPPED_ALLOCATOR_H

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include <memory/alloc.h>

using namespace testing;

TEST(bitmapped_allocator, create) {

  using namespace ZeroG::memory;

  bitmapped_allocator_create_info_t info;
  info.block_size = 64;
  info.block_alignment = 64;
  info.block_count = 1024;
  info.parent_allocator = nullptr;

  allocator_t *alloc = create(info);

  EXPECT_NE(alloc, nullptr);

  const uint64_t aligned_size = preferred_size(alloc, 32);

  EXPECT_EQ(aligned_size, 128);

  destroy(alloc);
}

TEST(bitmapped_allocator, allocate1) {

  using namespace ZeroG::memory;

  bitmapped_allocator_create_info_t info;
  info.block_size = 64;
  info.block_alignment = 64;
  info.block_count = 64;
  info.parent_allocator = nullptr;

  allocator_t *alloc = create(info);

  EXPECT_NE(alloc, nullptr);

  const block mem_blk = allocate(alloc, 32);

  EXPECT_EQ(mem_blk.size, 128);

  EXPECT_EQ((reinterpret_cast<uintptr_t>(mem_blk.address) & 63), 0);

  deallocate(alloc, mem_blk);

  destroy(alloc);
}

TEST(bitmapped_allocator, allocate2) {

  using namespace ZeroG::memory;

  bitmapped_allocator_create_info_t info;
  info.block_size = 64;
  info.block_alignment = 64;
  info.block_count = 64;
  info.parent_allocator = nullptr;

  allocator_t *alloc = create(info);

  EXPECT_NE(alloc, nullptr);

  const block mem_blk = allocate(alloc, 17);

  EXPECT_EQ(mem_blk.size, 128);

  EXPECT_EQ((reinterpret_cast<uintptr_t>(mem_blk.address) & 63), 0);

  deallocate(alloc, mem_blk);

  destroy(alloc);
}

TEST(bitmapped_allocator, allocate3) {

  using namespace ZeroG::memory;

  bitmapped_allocator_create_info_t info;
  info.block_size = 64;
  info.block_alignment = 64;
  info.block_count = 64;
  info.parent_allocator = nullptr;

  allocator_t *alloc = create(info);

  EXPECT_NE(alloc, nullptr);

  const block mem_blk_1 = allocate(alloc, 17);

  EXPECT_EQ(mem_blk_1.size, 128);

  EXPECT_EQ((reinterpret_cast<uintptr_t>(mem_blk_1.address) & 63), 0);

  const block mem_blk_2 = allocate(alloc, 17);

  EXPECT_EQ(mem_blk_2.size, 128);

  EXPECT_EQ((reinterpret_cast<uintptr_t>(mem_blk_2.address) & 63), 0);

  const block mem_blk_3 = allocate(alloc, 17);

  EXPECT_EQ(mem_blk_3.size, 128);

  EXPECT_EQ((reinterpret_cast<uintptr_t>(mem_blk_3.address) & 63), 0);

  deallocate(alloc, mem_blk_1);

  const block mem_blk_4 = allocate(alloc, 17);

  EXPECT_EQ(mem_blk_4.size, 128);

  EXPECT_EQ((reinterpret_cast<uintptr_t>(mem_blk_4.address) & 63), 0);

  EXPECT_EQ(mem_blk_4.address, mem_blk_1.address);

  deallocate(alloc, mem_blk_2);

  deallocate(alloc, mem_blk_3);

  deallocate(alloc, mem_blk_4);

  destroy(alloc);
}

TEST(bitmapped_allocator, allocate4) {

  using namespace ZeroG::memory;

  bitmapped_allocator_create_info_t info;
  info.block_size = 64;
  info.block_alignment = 64;
  info.block_count = 64;
  info.parent_allocator = nullptr;

  allocator_t *alloc = create(info);

  EXPECT_NE(alloc, nullptr);

  block allocs[64];

  for (int i = 0; i < 64; ++i) {
    allocs[i] = allocate(alloc, 17);

    EXPECT_EQ(allocs[i].size, 128);

    EXPECT_EQ((reinterpret_cast<uintptr_t>(allocs[i].address) & 63), 0);
  }

  const block blk = allocate(alloc, 17);

  EXPECT_EQ(blk.size, 0);
  EXPECT_EQ(blk.address, nullptr);

  for (int i = 0; i < 64; ++i) {
    deallocate(alloc, allocs[i]);
  }

  destroy(alloc);
}

#endif // TST_BITMAPPED_ALLOCATOR_H
