#ifndef TST_POOL_ALLOCATOR_H
#define TST_POOL_ALLOCATOR_H

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include <memory/alloc.h>

using namespace testing;

TEST(pool_allocator, create) {

  using namespace ZeroG::memory;

  pool_allocator_create_info_t info;
  info.block_size = 64;
  info.block_alignment = 64;
  info.block_count = 1024;
  info.parent_allocator = nullptr;

  allocator_t *alloc = create(info);

  EXPECT_NE(alloc, nullptr);

  const uint64_t aligned_size = preferred_size(alloc, 32);

  EXPECT_EQ(aligned_size, 64);

  destroy(alloc);
}

TEST(pool_allocator, allocate1) {

  using namespace ZeroG::memory;

  pool_allocator_create_info_t info;
  info.block_size = 64;
  info.block_alignment = 64;
  info.block_count = 1024;
  info.parent_allocator = nullptr;

  allocator_t *alloc = create(info);

  EXPECT_NE(alloc, nullptr);

  const block mem_blk = allocate(alloc, 32);

  EXPECT_EQ(mem_blk.size, 64);

  EXPECT_EQ((reinterpret_cast<uintptr_t>(mem_blk.address) & 63), 0);

  deallocate(alloc, mem_blk);

  destroy(alloc);
}

TEST(pool_allocator, allocate2) {

  using namespace ZeroG::memory;

  pool_allocator_create_info_t info;
  info.block_size = 64;
  info.block_alignment = 64;
  info.block_count = 1024;
  info.parent_allocator = nullptr;

  allocator_t *alloc = create(info);

  EXPECT_NE(alloc, nullptr);

  const block mem_blk = allocate(alloc, 17);

  EXPECT_EQ(mem_blk.size, 64);

  EXPECT_EQ((reinterpret_cast<uintptr_t>(mem_blk.address) & 63), 0);

  deallocate(alloc, mem_blk);

  destroy(alloc);
}

#endif // TST_POOL_ALLOCATOR_H
