#include "alloc.h"

#include <common/math.h>
#include <libdivide.h>

#include <atomic>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <memory>

#include <immintrin.h>

namespace ZeroG {
namespace memory {

enum allocator_type_t : size_t {
  STACK = 1,
  ATOMIC_STACK = 2,
  POOL = 3,
  ATOMIC_POOL = 4,
  BITMAPPED = 5,
  SECTION = 6
};

constexpr int64_t allocator_max_size{64};
using divider_t = libdivide::divider<uint32_t>;

union memory_address {
  void *ptr;
  uintptr_t addr;
};

struct allocator_t {
  allocator_type_t type;
  int64_t size;
  allocator_t *parent_allocator;
};

struct alignas(allocator_max_size) stack_allocator_t : allocator_t {
  int8_t *free_block_head;
  int8_t *memory_space_start;
  int8_t *memory_space_end;
  int32_t alignment;
  int8_t __padding[12];
};

struct alignas(allocator_max_size) atomic_stack_allocator_t : allocator_t {
  typedef int8_t *bype_ptr;
  std::atomic<bype_ptr> free_block_head;
  int8_t *memory_space_start;
  int8_t *memory_space_end;
  int32_t alignment;
  int8_t __padding[12];
};

struct alignas(allocator_max_size) pool_allocator_t : allocator_t {
  struct pool_block_t {
    pool_block_t *next;
  } * free_block_head;
  int64_t block_size;
  int8_t *memory_space_start;
  int32_t block_count;
  int32_t block_alignment;
  int8_t __padding[8];
};

struct alignas(allocator_max_size) atomic_pool_allocator_t : allocator_t {
  struct atomic_pool_block_t;
  typedef atomic_pool_block_t *atomic_pool_block_t_ptr;
  struct atomic_pool_block_t {
    std::atomic<atomic_pool_block_t_ptr> next;
  };
  std::atomic<atomic_pool_block_t_ptr> free_block_head;
  int64_t block_size;
  int8_t *memory_space_start;
  int32_t block_count;
  int32_t block_alignment;
  int8_t __padding[8];
};

using bitmapped_index_finder = int32_t (*)(int64_t *const control_bits,
                                           int32_t count);

struct alignas(allocator_max_size) bitmapped_allocator_t : allocator_t {
  uint64_t *control_bits;
  int8_t *memory_space_start;
  int32_t control_block_count;
  int32_t block_count;
  int32_t block_size;
  int32_t header_size;
  int32_t block_alignment;
  int8_t __padding[4];
};

struct alignas(allocator_max_size) section_allocator_t : allocator_t {
  int32_t block_count;
  int32_t block_size;
  divider_t block_divider;
  int32_t block_alignment;
  memory_address memory_space_start;
  memory_address control_block_start;
  memory_address free_block_head;
  memory_address control_blocks[];
};

struct bitmapped_header_section_t {
  int32_t idx : 24;
  int32_t elem : 8;
};

struct bitmapped_block_header_t {
  bitmapped_header_section_t lv1;
  bitmapped_header_section_t lv2;
  bitmapped_header_section_t lv3;
  bitmapped_header_section_t lv4;
};

struct aligned_storage_t {
  void *base_address;
  int64_t size;
  int8_t *usable_address;
  int64_t usable_size;
};

struct aligned_storage_create_info_t {
  allocator_t *parent_allocator;
  int64_t size;
  int64_t alignment;
};

#define LIKELY(expression) __builtin_expect(static_cast<bool>(expression), true)
#define UNLIKELY(expression)                                                   \
  __builtin_expect(static_cast<bool>(expression), false)

constexpr int64_t meta_bit_count{64};
constexpr int32_t meta_bit_count_small{meta_bit_count};
constexpr int32_t min_alignment{sizeof(uintptr_t)};
constexpr int32_t int64_t_size{sizeof(int64_t)};
constexpr int32_t bitmapped_block_header_size{sizeof(bitmapped_block_header_t)};
constexpr int32_t bitmapped_lv1_count{1};
constexpr int32_t bitmapped_lv2_count{64 * bitmapped_lv1_count};
constexpr int32_t bitmapped_lv3_count{64 * bitmapped_lv2_count};
constexpr int32_t bitmapped_idx_count{
    bitmapped_lv3_count + bitmapped_lv2_count + bitmapped_lv1_count};

static aligned_storage_t
create_aligned_storage(const aligned_storage_create_info_t &info) noexcept {
  void *address = nullptr;
  const int64_t alignment = info.alignment;
  const int64_t size =
      align_64(allocator_max_size + alignment * 2 + info.size, alignment);
  const uint64_t ualignment = static_cast<uint64_t>(alignment);
  const uint64_t usize = static_cast<uint64_t>(size);
  if (info.parent_allocator) {
    const block pb = allocate(info.parent_allocator, size);
    assert(pb.size == size && "Allocated size is equal to requested size");
    address = pb.address;
  } else {
    address = aligned_alloc(ualignment, usize);
  }

  void *usable_address = static_cast<int8_t *>(address) + allocator_max_size;
  uint64_t usable_size = (usize - allocator_max_size);
  std::align(ualignment, usize, usable_address, usable_size);

  assert(info.size <= usable_size && "Returned size is smaller then expected");
  return {address, size, static_cast<int8_t *>(usable_address),
          static_cast<int64_t>(usable_size)};
}

static void internal_clear_allocator(stack_allocator_t *alloc) noexcept {
  alloc->free_block_head = alloc->memory_space_start;
}

static void internal_clear_allocator(atomic_stack_allocator_t *alloc) noexcept {
  atomic_init(&(alloc->free_block_head), alloc->memory_space_start);
}

static void internal_clear_allocator(pool_allocator_t *alloc) noexcept {
  int8_t *current_address = alloc->memory_space_start;
  pool_allocator_t::pool_block_t *current =
      reinterpret_cast<pool_allocator_t::pool_block_t *>(current_address);
  alloc->free_block_head = current;
  for (int64_t i = 0, c = alloc->block_count - 1; i < c; ++i) {
    current_address += alloc->block_size;
    pool_allocator_t::pool_block_t *next =
        reinterpret_cast<pool_allocator_t::pool_block_t *>(current_address);
    current->next = next;
    current = next;
  }
  current->next = nullptr;
}

static void internal_clear_allocator(atomic_pool_allocator_t *alloc) noexcept {
  int8_t *current_address = alloc->memory_space_start;
  atomic_pool_allocator_t::atomic_pool_block_t_ptr current =
      reinterpret_cast<atomic_pool_allocator_t::atomic_pool_block_t_ptr>(
          current_address);
  atomic_init(&alloc->free_block_head, current);
  for (int64_t i = 0, c = alloc->block_count - 1; i < c; ++i) {
    current_address += alloc->block_size;
    atomic_pool_allocator_t::atomic_pool_block_t_ptr next =
        reinterpret_cast<atomic_pool_allocator_t::atomic_pool_block_t_ptr>(
            current_address);
    atomic_init(&current->next, next);
    current = next;
  }
  atomic_init(&current->next, nullptr);
}

static void internal_clear_allocator(bitmapped_allocator_t *alloc) noexcept {
  for (int32_t i = 0; i < alloc->control_block_count; ++i) {
    alloc->control_bits[i] = UINT64_MAX;
  }
}

static void internal_clear_allocator(section_allocator_t *alloc) noexcept {
  alloc->free_block_head.ptr = alloc->control_block_start.ptr;
  for (int32_t i = 0, l = alloc->block_count - 1; i < l; ++i) {
    alloc->control_blocks[i].addr =
        alloc->control_block_start.addr + (i + 1) * sizeof(memory_address);
  }
  alloc->control_blocks[alloc->block_count - 1].ptr = nullptr;
}

allocator_t *create(const stack_allocator_create_info_t &info) noexcept {
  const int32_t alignment = fast_max(info.alignment, min_alignment);
  const aligned_storage_t storage =
      create_aligned_storage({info.parent_allocator, info.size, alignment});
  stack_allocator_t *stack_alloc =
      static_cast<stack_allocator_t *>(storage.base_address);
  stack_alloc->type = STACK;
  stack_alloc->size = storage.size;
  stack_alloc->parent_allocator = info.parent_allocator;
  stack_alloc->alignment = alignment;
  stack_alloc->memory_space_start = storage.usable_address;
  stack_alloc->memory_space_end = storage.usable_address + storage.usable_size;

  internal_clear_allocator(stack_alloc);

  return stack_alloc;
}

allocator_t *create(const atomic_stack_allocator_create_info_t &info) noexcept {
  const int32_t alignment = fast_max(info.alignment, min_alignment);
  const aligned_storage_t storage =
      create_aligned_storage({info.parent_allocator, info.size, alignment});
  atomic_stack_allocator_t *stack_alloc =
      static_cast<atomic_stack_allocator_t *>(storage.base_address);
  stack_alloc->type = ATOMIC_STACK;
  stack_alloc->size = storage.size;
  stack_alloc->parent_allocator = info.parent_allocator;
  stack_alloc->alignment = alignment;
  stack_alloc->memory_space_start = storage.usable_address;
  stack_alloc->memory_space_end = storage.usable_address + storage.usable_size;

  internal_clear_allocator(stack_alloc);

  return stack_alloc;
}

allocator_t *create(const pool_allocator_create_info_t &info) noexcept {
  const int32_t alignment = fast_max(info.block_alignment, min_alignment);
  const int64_t aligned_block_size = align_64(info.block_size, alignment);
  const aligned_storage_t storage = create_aligned_storage(
      {info.parent_allocator, info.block_count * aligned_block_size,
       alignment});
  pool_allocator_t *pool_alloc =
      static_cast<pool_allocator_t *>(storage.base_address);
  pool_alloc->type = POOL;
  pool_alloc->size = storage.size;
  pool_alloc->parent_allocator = info.parent_allocator;
  pool_alloc->block_size = aligned_block_size;
  pool_alloc->block_count = info.block_count;
  pool_alloc->block_alignment = alignment;
  pool_alloc->memory_space_start = storage.usable_address;

  internal_clear_allocator(pool_alloc);

  return pool_alloc;
}

allocator_t *create(const atomic_pool_allocator_create_info_t &info) noexcept {
  const int32_t alignment = fast_max(info.block_alignment, min_alignment);
  const int64_t aligned_block_size = align_64(info.block_size, alignment);
  const aligned_storage_t storage = create_aligned_storage(
      {info.parent_allocator, info.block_count * aligned_block_size,
       alignment});
  atomic_pool_allocator_t *pool_alloc =
      static_cast<atomic_pool_allocator_t *>(storage.base_address);
  pool_alloc->type = ATOMIC_POOL;
  pool_alloc->size = storage.size;
  pool_alloc->parent_allocator = info.parent_allocator;
  pool_alloc->block_size = aligned_block_size;
  pool_alloc->block_count = info.block_count;
  pool_alloc->block_alignment = alignment;
  pool_alloc->memory_space_start = storage.usable_address;

  internal_clear_allocator(pool_alloc);

  return pool_alloc;
}

allocator_t *create(const bitmapped_allocator_create_info_t &info) noexcept {
  assert(info.block_count % meta_bit_count == 0 &&
         "Number of blocks must be a multiple of 64");
  const int32_t alignment = fast_max(info.block_alignment, allocator_max_size);
  const int32_t aligned_block_size = align_32(
      align_32(info.block_size, alignment) + bitmapped_block_header_size,
      alignment);
  const int32_t meta_array_size =
      bitmapped_idx_count + info.block_count / meta_bit_count_small;
  const int32_t aligned_meta_size =
      align_32(meta_array_size * int64_t_size, alignment);
  const int64_t required_size =
      info.block_count * aligned_block_size + aligned_meta_size;

  const aligned_storage_t storage =
      create_aligned_storage({info.parent_allocator, required_size, alignment});
  bitmapped_allocator_t *bitmapped_alloc =
      static_cast<bitmapped_allocator_t *>(storage.base_address);
  bitmapped_alloc->type = BITMAPPED;
  bitmapped_alloc->size = storage.size;
  bitmapped_alloc->parent_allocator = info.parent_allocator;
  bitmapped_alloc->control_bits =
      reinterpret_cast<uint64_t *>(storage.usable_address);
  bitmapped_alloc->block_size = aligned_block_size;
  bitmapped_alloc->block_count = info.block_count;
  bitmapped_alloc->control_block_count = meta_array_size;
  bitmapped_alloc->header_size =
      align_32(bitmapped_block_header_size, alignment);
  bitmapped_alloc->block_alignment = alignment;
  bitmapped_alloc->memory_space_start =
      storage.usable_address + aligned_meta_size;

  internal_clear_allocator(bitmapped_alloc);

  return bitmapped_alloc;
}

allocator_t *create(const section_allocator_create_info_t &info) noexcept {
  const int32_t alignment = fast_max(info.block_alignment, min_alignment);
  const int32_t aligned_block_size = align_32(info.block_size, alignment);
  const int32_t aligned_mete_size =
      align_32(info.block_count * sizeof(memory_address), allocator_max_size);
  const int64_t required_size =
      aligned_mete_size + info.block_count * aligned_block_size;

  const aligned_storage_t storage =
      create_aligned_storage({info.parent_allocator, required_size, alignment});
  section_allocator_t *section_alloc =
      static_cast<section_allocator_t *>(storage.base_address);
  section_alloc->type = SECTION;
  section_alloc->block_count = info.block_count;
  section_alloc->block_size = aligned_block_size;
  section_alloc->block_divider = aligned_block_size;
  section_alloc->block_alignment = alignment;
  section_alloc->memory_space_start.ptr =
      storage.usable_address + aligned_mete_size;
  section_alloc->control_block_start.ptr = storage.usable_address;
  section_alloc->free_block_head.ptr = &section_alloc->control_blocks[0];
  for (int32_t i = 0, l = info.block_count - 1; i < l; ++i) {
    section_alloc->control_blocks[i].ptr =
        storage.usable_address + (i + 1) * sizeof(memory_address);
  }
  section_alloc->control_blocks[info.block_count - 1].ptr = nullptr;
  return section_alloc;
}

void destroy(allocator_t *alloc) noexcept {
  assert(alloc != nullptr && "Null allocator");
  if (alloc->parent_allocator) {
    deallocate(alloc->parent_allocator, {alloc, alloc->size});
  } else {
    free(alloc);
  }
}

static block internal_allocate(stack_allocator_t *alloc,
                               int64_t size) noexcept {
  const int64_t aligned_size = align_64(size, alloc->alignment);
  int8_t *const current_free_block_head = alloc->free_block_head;
  int8_t *const next_free_block_head = current_free_block_head + aligned_size;
  if (next_free_block_head < alloc->memory_space_end) {
    alloc->free_block_head = next_free_block_head;
    return {current_free_block_head, aligned_size};
  }
  return {nullptr, 0};
}

static void internal_deallocate(stack_allocator_t *alloc, block blk) noexcept {
  int8_t *const prev_free_block_head = alloc->free_block_head - blk.size;
  if (prev_free_block_head == blk.address) {
    alloc->free_block_head = prev_free_block_head;
  }
}

static block internal_allocate(atomic_stack_allocator_t *alloc,
                               int64_t size) noexcept {
  const int64_t aligned_size = align_64(size, alloc->alignment);
  do {
    int8_t *current_free_block_head = atomic_load_explicit(
        &(alloc->free_block_head), std::memory_order_acquire);
    int8_t *const next_free_block_head = current_free_block_head + aligned_size;
    if (next_free_block_head >= alloc->memory_space_end) {
      break;
    }
    if (!atomic_compare_exchange_weak_explicit(
            &(alloc->free_block_head), &current_free_block_head,
            next_free_block_head, std::memory_order_acq_rel,
            std::memory_order_acquire)) {
      continue;
    }
    return {current_free_block_head, aligned_size};

  } while (true);
  return {nullptr, 0};
}

static void internal_deallocate(atomic_stack_allocator_t *alloc,
                                block blk) noexcept {
  int8_t *current_free_block_head = atomic_load_explicit(
      &(alloc->free_block_head), std::memory_order_acquire);
  int8_t *const prev_free_block_head = current_free_block_head - blk.size;
  if (prev_free_block_head == blk.address) {
    atomic_compare_exchange_weak_explicit(
        &(alloc->free_block_head), &current_free_block_head,
        prev_free_block_head, std::memory_order_acq_rel,
        std::memory_order_relaxed);
  }
}

block internal_allocate(pool_allocator_t *alloc, int64_t size) noexcept {
  const int64_t aligned_size = align_64(size, alloc->block_alignment);
  pool_allocator_t::pool_block_t *const current = alloc->free_block_head;
  __builtin_prefetch(current);
  __builtin_prefetch(alloc->free_block_head);
  __builtin_prefetch(current->next);
  if (UNLIKELY(current == nullptr) ||
      UNLIKELY(aligned_size > alloc->block_size)) {
    return {nullptr, 0};
  }
  alloc->free_block_head = current->next;
  return {current, aligned_size};
}

static void internal_deallocate(pool_allocator_t *alloc, block blk) noexcept {
  assert(blk.size == alloc->block_size && "Unexpected dealocation size");
  pool_allocator_t::pool_block_t *head_node =
      static_cast<pool_allocator_t::pool_block_t *>(blk.address);
  head_node->next = alloc->free_block_head;
  alloc->free_block_head = head_node;
}

static block internal_allocate(atomic_pool_allocator_t *alloc,
                               int64_t size) noexcept {
  const int64_t aligned_size = align_64(size, alloc->block_alignment);
  if (aligned_size == alloc->block_size) {
    atomic_pool_allocator_t::atomic_pool_block_t_ptr current =
        atomic_load_explicit(&(alloc->free_block_head),
                             std::memory_order_acquire);
    while (current != nullptr) {
      atomic_pool_allocator_t::atomic_pool_block_t_ptr next =
          atomic_load_explicit(&(current->next), std::memory_order_acquire);
      if (atomic_compare_exchange_weak_explicit(
              &(alloc->free_block_head), &current, next,
              std::memory_order_acq_rel, std::memory_order_acquire)) {
        break;
      }
    }
    return {current, current ? aligned_size : 0};
  }
  return {nullptr, 0};
}

static void internal_deallocate(atomic_pool_allocator_t *alloc,
                                block blk) noexcept {
  assert(blk.size == alloc->block_size && "Unexpected dealocation size");
  atomic_pool_allocator_t::atomic_pool_block_t_ptr free_node =
      static_cast<atomic_pool_allocator_t::atomic_pool_block_t_ptr>(
          blk.address);

  atomic_pool_allocator_t::atomic_pool_block_t_ptr current_node =
      atomic_load_explicit(&(alloc->free_block_head),
                           std::memory_order_acquire);
  int it = 0;
  do {
    atomic_store_explicit(&(free_node->next), current_node,
                          std::memory_order_release);
    ++it;
  } while (!atomic_compare_exchange_weak_explicit(
      &(alloc->free_block_head), &current_node, free_node,
      std::memory_order_acq_rel, std::memory_order_acquire));
}

static block internal_allocate(bitmapped_allocator_t *alloc,
                               int64_t size) noexcept {
  const int64_t aligned_size = align_64(size, alloc->block_alignment);
  const int64_t block_size = alloc->block_size;
  const int32_t header_size = alloc->header_size;
  const int32_t count = static_cast<int32_t>(alloc->control_block_count);
  const int32_t block_count = alloc->block_count;
  uint64_t *const control_bits = alloc->control_bits;
  int8_t *memory = alloc->memory_space_start;

  __builtin_prefetch(control_bits);
  __builtin_prefetch(&control_bits[bitmapped_idx_count]);

  if (UNLIKELY(block_size < aligned_size)) {
    return {nullptr, 0};
  }

  if (UNLIKELY(control_bits[0] == 0)) {
    return {nullptr, 0};
  }

  const int32_t lv1 = __builtin_ctzl(control_bits[0]);
  const int32_t idx2 = lv1 + 1;
  const int32_t lv2 = __builtin_ctzl(control_bits[idx2]);
  const int32_t idx3 = bitmapped_lv2_count * idx2 + lv2 + 1;
  const int32_t lv3 = __builtin_ctzl(control_bits[idx3]);
  const int32_t idx4 =
      bitmapped_lv3_count * idx2 + bitmapped_lv2_count * (lv2 + 1) + lv3 + 1;

  __builtin_prefetch(&control_bits[idx4]);

  if (LIKELY(idx4 >= count)) {
    return {nullptr, 0};
  }
  const int32_t offset = idx4 - bitmapped_idx_count;
  const int32_t lv4 = __builtin_ctzl(control_bits[idx4]);

  const int32_t elem_index = offset + lv4;
  const int64_t block_index = block_size * elem_index;

  if (UNLIKELY(elem_index >= block_count)) {
    return {nullptr, 0};
  }
  assert(is_aligned_64(block_index, alloc->block_alignment) &&
         "Block index is not alligned");

  int8_t *address = memory + block_index;
  void *return_address = address + header_size;

  __builtin_prefetch(address);
  __builtin_prefetch(return_address);

  control_bits[idx4] &= ~(1l << lv4);
  if (control_bits[idx4] == 0) {
    control_bits[idx3] &= ~(1l << lv3);
    if (control_bits[idx3] == 0) {
      control_bits[idx2] &= ~(1l << lv2);
      if (control_bits[idx2] == 0) {
        control_bits[0] &= ~(1l << lv1);
      }
    }
  }

  bitmapped_block_header_t *header =
      static_cast<bitmapped_block_header_t *>(static_cast<void *>(address));

  header->lv1 = {0, lv1};
  header->lv2 = {idx2, lv2};
  header->lv3 = {idx3, lv3};
  header->lv4 = {idx4, lv4};
  return {return_address, block_size};
}

static void internal_deallocate(bitmapped_allocator_t *alloc,
                                block blk) noexcept {
  if (blk.size == 0)
    return;
  assert(blk.size == alloc->block_size && "Unexpected dealocation size");
  const int8_t *address =
      static_cast<int8_t *>(blk.address) - alloc->header_size;
  const bitmapped_block_header_t *header =
      static_cast<const bitmapped_block_header_t *>(
          static_cast<const void *>(address));
  assert(address >= alloc->memory_space_start &&
         address <
             alloc->memory_space_start + alloc->size - alloc->block_size &&
         "Unexpexted dealocation pointer");
  const bitmapped_header_section_t lv1 = header->lv1;
  const bitmapped_header_section_t lv2 = header->lv2;
  const bitmapped_header_section_t lv3 = header->lv3;
  const bitmapped_header_section_t lv4 = header->lv4;

  alloc->control_bits[lv4.idx] |= (1l << lv4.elem);
  alloc->control_bits[lv3.idx] |= (1l << lv3.elem);
  alloc->control_bits[lv2.idx] |= (1l << lv2.elem);
  alloc->control_bits[lv1.idx] |= (1l << lv1.elem);
}

static block internal_allocate(section_allocator_t *alloc,
                               int64_t size) noexcept {
  const int64_t aligned_size = align_64(size, alloc->block_alignment);
  if (UNLIKELY(alloc->block_size - aligned_size < 0)) {
    assert(!"Requested allocation is too large");
    return {nullptr, 0};
  }

  const memory_address start = alloc->control_block_start;
  const memory_address current = alloc->free_block_head;
  if (UNLIKELY(current.ptr == nullptr))
    return {nullptr, 0};

  const int32_t idx = (current.addr - start.addr) >> 3;
  const memory_address res = {.addr = alloc->memory_space_start.addr +
                                      idx * alloc->block_size};

  memory_address *next = static_cast<memory_address *>(current.ptr);
  alloc->free_block_head.ptr = next->ptr;
  return {res.ptr, alloc->block_size};
}

static void internal_deallocate(section_allocator_t *alloc,
                                block blk) noexcept {
  assert(blk.size == alloc->block_size && "Unexpected dealocation size");
  const memory_address current = {blk.address};
  const memory_address start = alloc->memory_space_start;

  const int32_t idx =
      uint32_t(current.addr - start.addr) / alloc->block_divider - 1;
  alloc->control_blocks[idx].ptr = current.ptr;
  alloc->free_block_head.ptr = &alloc->control_blocks[idx];
}

block allocate(allocator_t *alloc, int64_t size) noexcept {
  assert(alloc != nullptr && "Null allocator");
  switch (alloc->type) {
  case STACK: {
    return internal_allocate(static_cast<stack_allocator_t *>(alloc), size);
  }
  case ATOMIC_STACK: {
    return internal_allocate(static_cast<atomic_stack_allocator_t *>(alloc),
                             size);
  }
  case POOL: {
    return internal_allocate(static_cast<pool_allocator_t *>(alloc), size);
  }
  case ATOMIC_POOL: {
    return internal_allocate(static_cast<atomic_pool_allocator_t *>(alloc),
                             size);
  }
  case BITMAPPED: {
    return internal_allocate(static_cast<bitmapped_allocator_t *>(alloc), size);
  }
  case SECTION: {
    return internal_allocate(static_cast<section_allocator_t *>(alloc), size);
  }
  }
  return {nullptr, 0};
}

void deallocate(allocator_t *alloc, block blk) noexcept {
  assert(alloc != nullptr && "Null allocator");
  switch (alloc->type) {
  case STACK: {
    internal_deallocate(static_cast<stack_allocator_t *>(alloc), blk);
    break;
  }
  case ATOMIC_STACK: {
    internal_deallocate(static_cast<atomic_stack_allocator_t *>(alloc), blk);
    break;
  }
  case POOL: {
    internal_deallocate(static_cast<pool_allocator_t *>(alloc), blk);
    break;
  }
  case ATOMIC_POOL: {
    internal_deallocate(static_cast<atomic_pool_allocator_t *>(alloc), blk);
    break;
  }
  case BITMAPPED: {
    internal_deallocate(static_cast<bitmapped_allocator_t *>(alloc), blk);
    break;
  }
  case SECTION: {
    internal_deallocate(static_cast<section_allocator_t *>(alloc), blk);
    break;
  }
  }
}

void deallocate_all(allocator_t *alloc) noexcept {
  assert(alloc != nullptr && "Null allocator");
  switch (alloc->type) {
  case STACK: {
    internal_clear_allocator(static_cast<stack_allocator_t *>(alloc));
    break;
  }
  case ATOMIC_STACK: {
    internal_clear_allocator(static_cast<atomic_stack_allocator_t *>(alloc));
    break;
  }
  case POOL: {
    internal_clear_allocator(static_cast<pool_allocator_t *>(alloc));
    break;
  }
  case ATOMIC_POOL: {
    internal_clear_allocator(static_cast<atomic_pool_allocator_t *>(alloc));
    break;
  }
  case BITMAPPED: {
    internal_clear_allocator(static_cast<bitmapped_allocator_t *>(alloc));
    break;
  }
  case SECTION: {
    internal_clear_allocator(static_cast<section_allocator_t *>(alloc));
    break;
  }
  }
}

int64_t preferred_size(allocator_t *alloc, int64_t size) noexcept {
  assert(alloc != nullptr && "Null allocator");
  switch (alloc->type) {
  case STACK: {
    const stack_allocator_t *const stack_alloc =
        static_cast<stack_allocator_t *>(alloc);
    return align_64(size, fast_max(stack_alloc->alignment, min_alignment));
  }
  case ATOMIC_STACK: {
    const atomic_stack_allocator_t *const stack_alloc =
        static_cast<atomic_stack_allocator_t *>(alloc);
    return align_64(size, fast_max(stack_alloc->alignment, min_alignment));
  }
  case POOL: {
    const pool_allocator_t *const pool_alloc =
        static_cast<pool_allocator_t *>(alloc);
    const int64_t aligned_size =
        align_64(size, fast_max(pool_alloc->block_alignment, min_alignment));
    return aligned_size == pool_alloc->block_size ? aligned_size : 0;
  }
  case ATOMIC_POOL: {
    const atomic_pool_allocator_t *const pool_alloc =
        static_cast<atomic_pool_allocator_t *>(alloc);
    const int64_t aligned_size =
        align_64(size, fast_max(pool_alloc->block_alignment, min_alignment));
    return aligned_size == pool_alloc->block_size ? aligned_size : 0;
  }
  case BITMAPPED: {
    const bitmapped_allocator_t *const bitmapped_alloc =
        static_cast<bitmapped_allocator_t *>(alloc);
    const int32_t alignment =
        fast_max(bitmapped_alloc->block_alignment, min_alignment);
    const int64_t aligned_size = align_64(
        align_64(size, alignment) + bitmapped_block_header_size, alignment);
    return aligned_size == bitmapped_alloc->block_size ? aligned_size : 0;
  }
  case SECTION: {
    const section_allocator_t *const section_alloc =
        static_cast<section_allocator_t *>(alloc);
    return section_alloc->block_size;
  }
  }
  return 0;
}

} // namespace memory
} // namespace ZeroG
