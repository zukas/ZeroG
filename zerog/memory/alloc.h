#ifndef ALLOC_H
#define ALLOC_H

#include <common/types.h>

namespace ZeroG {
namespace memory {

constexpr int32_t B{1};
constexpr int32_t KB{B * 1024};
constexpr int32_t MB{KB * 1024};
constexpr int32_t GB{MB * 1024};

constexpr int64_t align_64(int64_t size, int64_t alignment) noexcept {
  const int64_t temp_alignment = alignment - 1;
  return (size + temp_alignment) & ~temp_alignment;
}

constexpr int32_t align_32(int32_t size, int32_t alignment) noexcept {
  const int32_t temp_alignment = alignment - 1;
  return (size + temp_alignment) & ~temp_alignment;
}

constexpr int16_t align_16(int16_t size, int16_t alignment) noexcept {
  const int16_t temp_alignment = alignment - 1;
  return (size + temp_alignment) & ~temp_alignment;
}

constexpr bool is_aligned_64(int64_t size, int64_t alignment) noexcept {
  return (size & (alignment - 1)) == 0;
}

constexpr bool is_aligned_32(int32_t size, int32_t alignment) noexcept {
  return (size & (alignment - 1)) == 0;
}

constexpr bool is_aligned_16(int16_t size, int16_t alignment) noexcept {
  return (size & (alignment - 1)) == 0;
}

typedef struct allocator_t allocator_t;

struct stack_allocator_create_info_t {
  allocator_t *parent_allocator{nullptr};
  int64_t size{0};
  int32_t alignment{0};
};

struct atomic_stack_allocator_create_info_t {
  allocator_t *parent_allocator{nullptr};
  int64_t size{0};
  int32_t alignment{0};
};

struct pool_allocator_create_info_t {
  allocator_t *parent_allocator{nullptr};
  int32_t block_size{0};
  int32_t block_alignment{0};
  int32_t block_count{0};
};

struct atomic_pool_allocator_create_info_t {
  allocator_t *parent_allocator{nullptr};
  int32_t block_size{0};
  int32_t block_alignment{0};
  int32_t block_count{0};
};

struct bitmapped_allocator_create_info_t {
  allocator_t *parent_allocator{nullptr};
  int32_t block_size{0};
  int32_t block_alignment{0};
  int32_t block_count{0};
};

struct section_allocator_create_info_t {
  allocator_t *parent_allocator{nullptr};
  int32_t block_size{0};
  int32_t block_alignment{0};
  int32_t block_count{0};
};

struct block {
  void *address{nullptr};
  int64_t size{0};
};

allocator_t *create(const stack_allocator_create_info_t &info) noexcept;

allocator_t *create(const atomic_stack_allocator_create_info_t &info) noexcept;

allocator_t *create(const pool_allocator_create_info_t &info) noexcept;

allocator_t *create(const atomic_pool_allocator_create_info_t &info) noexcept;

allocator_t *create(const bitmapped_allocator_create_info_t &info) noexcept;

allocator_t *create(const section_allocator_create_info_t &info) noexcept;

void destroy(allocator_t *alloc) noexcept;

block allocate(allocator_t *alloc, int64_t size) noexcept;

void deallocate(allocator_t *alloc, block blk) noexcept;

void deallocate_all(allocator_t *alloc) noexcept;

int64_t preferred_size(allocator_t *alloc, int64_t size) noexcept;

} // namespace memory

} // namespace ZeroG

#endif // ALLOC_H
