cmake_minimum_required(VERSION 2.8)

project(memory-perf)
add_executable(${PROJECT_NAME} main.cpp singlethreaded_tests.h multithreaded_tests.h )
target_link_libraries(${PROJECT_NAME} benchmark memory ${GLOBAL_LIBS})

