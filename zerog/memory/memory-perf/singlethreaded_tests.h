#ifndef SINGLETHREADED_TESTS_H
#define SINGLETHREADED_TESTS_H

#include <benchmark/benchmark.h>

#include <memory/alloc.h>
#include <memory_resource>

template <typename _AlocType, typename _AlocFunc, typename _DealocFunc>
static double loop_iteration(_AlocFunc &&alloc_func, _DealocFunc &&dealloc_func,
                             int64_t size, int64_t count) {

  std::vector<_AlocType> allocs;
  allocs.reserve(static_cast<size_t>(count));
  auto start = std::chrono::high_resolution_clock::now();
  for (int64_t i = 0; i < count; ++i) {
    allocs.push_back(alloc_func(size));
  }
  benchmark::DoNotOptimize(allocs.data());
  for (int64_t i = count - 1; i >= 0; --i) {
    const size_t idx = static_cast<size_t>(i);
    dealloc_func(allocs[idx]);
  }
  auto end = std::chrono::high_resolution_clock::now();
  auto duration = end - start;
  return std::chrono::duration_cast<std::chrono::nanoseconds>(duration)
             .count() /
         1000000000.0;
}

#define CUSTOM_BENCHMARK(function)                                             \
  BENCHMARK(function)                                                          \
      ->Args({64, 32})                                                         \
      ->Args({64, 128})                                                        \
      ->Args({64, 1024})                                                       \
      ->Args({64, 8192})                                                       \
      ->Args({256, 32})                                                        \
      ->Args({256, 128})                                                       \
      ->Args({256, 1024})                                                      \
      ->Args({256, 8192})                                                      \
      ->Args({1024, 32})                                                       \
      ->Args({1024, 128})                                                      \
      ->Args({1024, 1024})                                                     \
      ->Args({1024, 8192})                                                     \
      ->Args({4096, 32})                                                       \
      ->Args({4096, 128})                                                      \
      ->Args({4096, 1024})                                                     \
      ->Args({4096, 8192})                                                     \
      ->Args({16384, 32})                                                      \
      ->Args({16384, 128})                                                     \
      ->Args({16384, 1024})                                                    \
      ->Args({16384, 8192})                                                    \
      ->UseManualTime()                                                        \
      ->Unit(benchmark::kNanosecond)

static void test_malloc(benchmark::State &state) {

  const int32_t count = static_cast<int32_t>(state.range(0));
  const int32_t size = static_cast<int32_t>(state.range(1));

  for (auto _ : state) {
    state.SetIterationTime(
        loop_iteration<void *>([](size_t size) { return malloc(size); },
                               [](void *p) { free(p); }, size, count));
    benchmark::ClobberMemory();
  }
  state.counters["One"] = benchmark::Counter(count * state.iterations(),
                                             benchmark::Counter::kIsRate |
                                                 benchmark::Counter::kInvert);
}

static void test_aligned_malloc(benchmark::State &state) {

  const int32_t count = static_cast<int32_t>(state.range(0));
  const int32_t size = static_cast<int32_t>(state.range(1));

  for (auto _ : state) {
    state.SetIterationTime(loop_iteration<void *>(
        [](size_t size) { return aligned_alloc(8, size); },
        [](void *p) { free(p); }, size, count));
    benchmark::ClobberMemory();
  }
  state.counters["One"] = benchmark::Counter(count * state.iterations(),
                                             benchmark::Counter::kIsRate |
                                                 benchmark::Counter::kInvert);
}

static void test_pmt_synchronized_pool_resource(benchmark::State &state) {

  using namespace ZeroG::memory;

  const int32_t count = static_cast<int32_t>(state.range(0));
  const int32_t size = static_cast<int32_t>(state.range(1));
  const int32_t threads = static_cast<int32_t>(state.range(2));

  std::pmr::pool_options options;
  options.max_blocks_per_chunk = static_cast<size_t>(count * threads);
  options.largest_required_pool_block = static_cast<size_t>(size);

  std::pmr::unsynchronized_pool_resource alloc(options);
  benchmark::ClobberMemory();

  for (auto _ : state) {
    state.SetIterationTime(loop_iteration<block>(
        [&alloc](size_t size) {
          return block{alloc.allocate(size), static_cast<int64_t>(size)};
        },
        [&alloc](block blk) {
          alloc.deallocate(blk.address, static_cast<size_t>(blk.size));
        },
        size, count));
    benchmark::ClobberMemory();
  }
  state.counters["One"] = benchmark::Counter(count * state.iterations(),
                                             benchmark::Counter::kIsRate |
                                                 benchmark::Counter::kInvert);
}

static void test_stack_allocator(benchmark::State &state) {
  using namespace ZeroG::memory;

  const int32_t count = static_cast<int32_t>(state.range(0));
  const int32_t size = static_cast<int32_t>(state.range(1));

  stack_allocator_create_info_t info;
  info.parent_allocator = nullptr;
  info.size = count * ((size + 7) & ~7l);
  info.alignment = 8;

  allocator_t *alloc = create(info);

  for (auto _ : state) {
    state.SetIterationTime(loop_iteration<block>(
        [alloc](int64_t size) { return allocate(alloc, size); },
        [alloc](block blk) { deallocate(alloc, blk); }, size, count));
    deallocate_all(alloc);
    benchmark::ClobberMemory();
  }
  state.counters["One"] = benchmark::Counter(count * state.iterations(),
                                             benchmark::Counter::kIsRate |
                                                 benchmark::Counter::kInvert);
}

static void test_pool_allocator(benchmark::State &state) {
  using namespace ZeroG::memory;

  const int32_t count = static_cast<int32_t>(state.range(0));
  const int32_t size = static_cast<int32_t>(state.range(1));

  pool_allocator_create_info_t info;
  info.parent_allocator = nullptr;
  info.block_size = size;
  info.block_count = count;
  info.block_alignment = 8;

  allocator_t *alloc = create(info);

  for (auto _ : state) {
    state.SetIterationTime(loop_iteration<block>(
        [alloc](int64_t size) { return allocate(alloc, size); },
        [alloc](block blk) { deallocate(alloc, blk); }, size, count));
    deallocate_all(alloc);
    benchmark::ClobberMemory();
  }
  state.counters["One"] = benchmark::Counter(count * state.iterations(),
                                             benchmark::Counter::kIsRate |
                                                 benchmark::Counter::kInvert);
}

static void test_bitmapped_allocator(benchmark::State &state) {
  using namespace ZeroG::memory;

  const int32_t count = static_cast<int32_t>(state.range(0));
  const int32_t size = static_cast<int32_t>(state.range(1));

  bitmapped_allocator_create_info_t info;
  info.parent_allocator = nullptr;
  info.block_size = size;
  info.block_count = count;
  info.block_alignment = 8;

  allocator_t *alloc = create(info);

  for (auto _ : state) {
    state.SetIterationTime(loop_iteration<block>(
        [alloc](int64_t size) { return allocate(alloc, size); },
        [alloc](block blk) { deallocate(alloc, blk); }, size, count));
    deallocate_all(alloc);
    benchmark::ClobberMemory();
  }
  state.counters["One"] = benchmark::Counter(count * state.iterations(),
                                             benchmark::Counter::kIsRate |
                                                 benchmark::Counter::kInvert);
}

static void test_section_allocator(benchmark::State &state) {
  using namespace ZeroG::memory;

  const int32_t count = static_cast<int32_t>(state.range(0));
  const int32_t size = static_cast<int32_t>(state.range(1));

  section_allocator_create_info_t info;
  info.parent_allocator = nullptr;
  info.block_size = size;
  info.block_count = count;
  info.block_alignment = 8;

  allocator_t *alloc = create(info);

  for (auto _ : state) {
    state.SetIterationTime(loop_iteration<block>(
        [alloc](int64_t size) { return allocate(alloc, size); },
        [alloc](block blk) { deallocate(alloc, blk); }, size, count));
    deallocate_all(alloc);
    benchmark::ClobberMemory();
  }
  state.counters["One"] = benchmark::Counter(count * state.iterations(),
                                             benchmark::Counter::kIsRate |
                                                 benchmark::Counter::kInvert);
}

CUSTOM_BENCHMARK(test_malloc);
CUSTOM_BENCHMARK(test_aligned_malloc);
CUSTOM_BENCHMARK(test_pmt_synchronized_pool_resource);
CUSTOM_BENCHMARK(test_stack_allocator);
CUSTOM_BENCHMARK(test_pool_allocator);
CUSTOM_BENCHMARK(test_bitmapped_allocator);
CUSTOM_BENCHMARK(test_section_allocator);

#endif // SINGLETHREADED_TESTS_H
