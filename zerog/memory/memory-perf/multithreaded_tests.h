#ifndef MULTITHREADED_TESTS_H
#define MULTITHREADED_TESTS_H

#include <benchmark/benchmark.h>

#include <memory/alloc.h>
#include <memory_resource>

#include <chrono>
#include <future>
#include <thread>

template <typename _AlocType, typename _AlocFunc, typename _DealocFunc>
static double threaded_loop_iteration(_AlocFunc &&alloc_func,
                                      _DealocFunc &&dealloc_func, int64_t size,
                                      int64_t count, int64_t threads) {

  struct reverse {
    typedef std::vector<std::future<int64_t>> __T;
    reverse(__T &vec) : _begin(vec.rbegin()), _end(vec.rend()) {}
    __T::reverse_iterator _begin, _end;

    __T::reverse_iterator begin() const { return _begin; }
    __T::reverse_iterator end() const { return _end; }
  };

  std::vector<std::future<int64_t>> futures(static_cast<size_t>(threads));
  for (auto &th : futures) {
    th = std::async(
        std::launch::async,
        [](_AlocFunc alloc_func, _DealocFunc dealloc_func, int64_t size,
           int64_t count) {
          std::vector<_AlocType> allocs;
          allocs.reserve(static_cast<size_t>(count));
          auto start = std::chrono::high_resolution_clock::now();
          for (int64_t i = 0; i < count; ++i) {
            allocs.push_back(alloc_func(size));
          }
          benchmark::DoNotOptimize(allocs.data());
          for (int64_t i = count - 1; i >= 0; --i) {
            const size_t idx = static_cast<size_t>(i);
            dealloc_func(allocs[idx]);
          }
          auto end = std::chrono::high_resolution_clock::now();
          auto duration = end - start;
          return std::chrono::duration_cast<std::chrono::nanoseconds>(duration)
              .count();
        },
        alloc_func, dealloc_func, size, count);
  }

  int64_t time_spent = 0;
  for (auto &th : reverse(futures)) {
    time_spent += th.get();
  }
  return double(time_spent) / 1000000000.0;
}

#define CUSTOM_BENCHMARK_THREAD(function, threads)                             \
  BENCHMARK(function)                                                          \
      ->Args({64, 32, threads})                                                \
      ->Args({64, 128, threads})                                               \
      ->Args({64, 1024, threads})                                              \
      ->Args({64, 8192, threads})                                              \
      ->Args({256, 32, threads})                                               \
      ->Args({256, 128, threads})                                              \
      ->Args({256, 1024, threads})                                             \
      ->Args({256, 8192, threads})                                             \
      ->Args({1024, 32, threads})                                              \
      ->Args({1024, 128, threads})                                             \
      ->Args({1024, 1024, threads})                                            \
      ->Args({1024, 8192, threads})                                            \
      ->Args({4096, 32, threads})                                              \
      ->Args({4096, 128, threads})                                             \
      ->Args({4096, 1024, threads})                                            \
      ->Args({4096, 8192, threads})                                            \
      ->Args({16384, 32, threads})                                             \
      ->Args({16384, 128, threads})                                            \
      ->Args({16384, 1024, threads})                                           \
      ->Args({16384, 8192, threads})                                           \
      ->UseManualTime()                                                        \
      ->Unit(benchmark::kNanosecond)

static void test_malloc_multithread(benchmark::State &state) {

  const int32_t count = static_cast<int32_t>(state.range(0));
  const int32_t size = static_cast<int32_t>(state.range(1));
  const int32_t threads = static_cast<int32_t>(state.range(2));

  for (auto _ : state) {
    state.SetIterationTime(threaded_loop_iteration<void *>(
        [](size_t size) { return malloc(size); }, [](void *p) { free(p); },
        size, count, threads));
    benchmark::ClobberMemory();
  }
}

static void test_aligned_malloc_multithread(benchmark::State &state) {

  const int32_t count = static_cast<int32_t>(state.range(0));
  const int32_t size = static_cast<int32_t>(state.range(1));
  const int32_t threads = static_cast<int32_t>(state.range(2));

  for (auto _ : state) {
    state.SetIterationTime(threaded_loop_iteration<void *>(
        [](size_t size) { return aligned_alloc(8, size); },
        [](void *p) { free(p); }, size, count, threads));
    benchmark::ClobberMemory();
  }
}

static void test_atomic_stack_alloc_multithread(benchmark::State &state) {

  using namespace ZeroG::memory;

  const int32_t count = static_cast<int32_t>(state.range(0));
  const int32_t size = static_cast<int32_t>(state.range(1));
  const int32_t threads = static_cast<int32_t>(state.range(2));

  atomic_stack_allocator_create_info_t info;
  info.parent_allocator = nullptr;
  info.size = count * threads * ((size + 7) & ~7l);
  info.alignment = 8;

  allocator_t *alloc = create(info);

  benchmark::DoNotOptimize(alloc);
  benchmark::ClobberMemory();

  for (auto _ : state) {
    state.SetIterationTime(threaded_loop_iteration<block>(
        [alloc](int64_t size) { return allocate(alloc, size); },
        [alloc](block blk) { deallocate(alloc, blk); }, size, count, threads));
    benchmark::ClobberMemory();
    deallocate_all(alloc);
  }

  destroy(alloc);
}

static void
test_pmt_synchronized_pool_resource_multithread(benchmark::State &state) {

  using namespace ZeroG::memory;

  const int32_t count = static_cast<int32_t>(state.range(0));
  const int32_t size = static_cast<int32_t>(state.range(1));
  const int32_t threads = static_cast<int32_t>(state.range(2));

  std::pmr::pool_options options;
  options.max_blocks_per_chunk = static_cast<size_t>(count * threads);
  options.largest_required_pool_block = static_cast<size_t>(size);

  std::pmr::synchronized_pool_resource alloc(options);
  benchmark::ClobberMemory();

  for (auto _ : state) {
    state.SetIterationTime(threaded_loop_iteration<block>(
        [&alloc](size_t size) {
          return block{alloc.allocate(size), static_cast<int64_t>(size)};
        },
        [&alloc](block blk) {
          alloc.deallocate(blk.address, static_cast<size_t>(blk.size));
        },
        size, count, threads));
    benchmark::ClobberMemory();
  }
}

static void test_atomic_pool_alloc_multithread(benchmark::State &state) {

  using namespace ZeroG::memory;

  const int32_t count = static_cast<int32_t>(state.range(0));
  const int32_t size = static_cast<int32_t>(state.range(1));
  const int32_t threads = static_cast<int32_t>(state.range(2));

  atomic_pool_allocator_create_info_t info;
  info.parent_allocator = nullptr;
  info.block_size = size;
  info.block_count = count * threads;
  info.block_alignment = 8;

  allocator_t *alloc = create(info);

  benchmark::DoNotOptimize(alloc);
  benchmark::ClobberMemory();

  for (auto _ : state) {
    state.SetIterationTime(threaded_loop_iteration<block>(
        [alloc](int64_t size) { return allocate(alloc, size); },
        [alloc](block blk) { deallocate(alloc, blk); }, size, count, threads));
    benchmark::ClobberMemory();
    deallocate_all(alloc);
  }

  destroy(alloc);
}

CUSTOM_BENCHMARK_THREAD(test_malloc_multithread, 2);
CUSTOM_BENCHMARK_THREAD(test_aligned_malloc_multithread, 2);
CUSTOM_BENCHMARK_THREAD(test_pmt_synchronized_pool_resource_multithread, 2);
CUSTOM_BENCHMARK_THREAD(test_atomic_stack_alloc_multithread, 2);
CUSTOM_BENCHMARK_THREAD(test_atomic_pool_alloc_multithread, 2);

CUSTOM_BENCHMARK_THREAD(test_malloc_multithread, 4);
CUSTOM_BENCHMARK_THREAD(test_aligned_malloc_multithread, 4);
CUSTOM_BENCHMARK_THREAD(test_pmt_synchronized_pool_resource_multithread, 4);
CUSTOM_BENCHMARK_THREAD(test_atomic_stack_alloc_multithread, 4);
CUSTOM_BENCHMARK_THREAD(test_atomic_pool_alloc_multithread, 4);

CUSTOM_BENCHMARK_THREAD(test_malloc_multithread, 8);
CUSTOM_BENCHMARK_THREAD(test_aligned_malloc_multithread, 8);
CUSTOM_BENCHMARK_THREAD(test_pmt_synchronized_pool_resource_multithread, 8);
CUSTOM_BENCHMARK_THREAD(test_atomic_stack_alloc_multithread, 8);
CUSTOM_BENCHMARK_THREAD(test_atomic_pool_alloc_multithread, 8);

#endif // MULTITHREADED_TESTS_H
