#ifndef IMAGE_SOURCE_H
#define IMAGE_SOURCE_H

#include <common/types.h>

namespace ZeroG {

namespace image_source {

struct image {
  void *data;
  uint32_t width;
  uint32_t height;
  graphics::texture_format format;
};

image load(const char *filename);

void unload(image image);

} // namespace image_source

} // namespace ZeroG

#endif // IMAGE_SOURCE_H
