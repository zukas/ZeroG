#version 450
#pragma debug(on)

in block
{
    layout(location = 1) vec4 vColour;
} In;

layout(location = 0, index = 0) out vec4 fColour;

void main(void)
{
    fColour = In.vColour;
}
