#version 450

in block
{
    layout(location = 1) vec3 vNorm;
    layout(location = 2) vec2 vUV;
} In;

layout(location = 7, binding = 0) uniform sampler2D textureSampler;

layout(location = 0, index = 0) out vec4 fColour;

void main(void)
{   
    fColour = texture2D(textureSampler, vec2(In.vUV.x, In.vUV.y));
}
