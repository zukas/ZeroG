#version 450 core
#pragma debug(on)

layout(location = 0) in vec4 vPos;
layout(location = 1) in vec4 vColour;

out gl_PerVertex
{
    layout(location = 0) vec4 gl_Position;
};

out block
{
    layout(location = 1) vec4 vColour;
} Out;

void main(){
    gl_Position = vPos;
    Out.vColour = vColour;
}
