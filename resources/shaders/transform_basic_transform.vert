#version 450 core
#pragma debug(on)

layout(location = 0) in vec3 vPos;
layout(location = 1) in vec3 vColour;
layout(location = 2) in mat4 vMVP;

out gl_PerVertex
{
    layout(location = 0) vec4 gl_Position;
};

out block
{
    layout(location = 1) vec4 vColour;
} Out;

void main(){
    gl_Position = vMVP * vec4(vPos, 1.0);
    Out.vColour = vec4(vColour,1.0);
}
